package edu.belmont.csc.src.lists;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<T> {

    // TODO: Complete this class below
    // Define the internal Node type
    public class Node<T> {
        // class variables for Node
        public Node next;
        public T data;

        // constructor
        public Node(T data, Node next)
        {
            this.data = data;
            this.next = next;
        }

        public Node(T data)
        {
            this(data, null);
        }
    }
    // End of TODO


    // TODO: Declare the class variables below
    // class variables for SinglyLinkedListClass
    public Node head = null;

    // End of TODO


    public void insertAtHead(T data) {
        Node node = new Node(data);

        if (head != null) {
            node.next = head;
        }

        head = node;
    }

    public void insertAfter(Node prev, T data) {
        if (prev.data == null)
            throw new NullPointerException("Previous node has null data");

        Node node = new Node(data);
        node.next = prev.next;
        prev.next = node;
    }

    // TODO: Complete this method below
    /**
     * This method inserts a new node containing data at the specified position
     * @param data: data of the new node
     * @param index: the position where the new node should be inserted
     */
    public void insertAt(T data, int index) {
        Node prev = head;
        // if list is empty
        if (head == null && index != 0)
            throw new IndexOutOfBoundsException("Index " + index + " invalid for empty list");

        // if index is 0, it is equivalent to insert at head
        else if (index == 0)
            head = new Node(data);

        // otherwise...
        else {
            for (int i = 1; i < index; i++)
                prev = prev.next;

            insertAfter(prev, data);
        }
    }
    // End of TODO


    public void append(T data) {
        Node node = head;

        if (head.data == null)
            insertAtHead(data);
        else {
            while (node.next != null)
                node = node.next;

            insertAfter(node, data);
        }
    }

    // TODO: Complete this method below
    public void printList() {
        Node node = head;
        while(node.next != null) {
            System.out.println(node.data);
            node = node.next;
        }
        System.out.println(node.data);
    }
    // End of TODO

    // TODO: Complete this method below
    public Node removeFromHead() {
        head = head.next;
        return head;
    }
    // End of TODO

    // TODO: Complete this method below
    public Node removeFromTail() {
        Node prev = head;
        Node node = head;
        while (node.next != null) {
            prev = node;
            node = node.next;
        }

        prev.next = null;
        return node;
    }
    // End of TODO

    public Node removeAt(int index) {
        Node node = head;
        Node prev = node;
        Node next = node.next;

        //for (int i = 1; i < index; i++)
        int i = 1;
        while (i < index && next != null) {
            prev = node;
            node = next;
            next = node.next;

            i++;
        }

        Node ret = next;
        if (ret.next != null)
            node.next = ret.next;

        return ret;
    }

}