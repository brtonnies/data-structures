package edu.belmont.csc.src.lists;

import edu.belmont.csc.src.lists.nodes.Node;

public class LinkedList<T> {
    public Node head = null;
    public Node tail = null;


    // Empty the linked list; this should take O(n) time
    public void clear()
    {
        Node node = tail;
        tail = null;

        while(node.getPrev() != null) {
            node = node.getPrev();
            node.setNext(null);
        }

        head = null;
    }


    // Check if the linked list if empty
    public boolean isEmpty() {
        return head == null && tail == null;
    }

    // Append a node to the tail of the linked list; this should have an O(1) implementation
    public void append(T data)
    {
        Node node = new Node(data);

        if (tail == null) {
            tail = node;

            if (head == null)
                head = node;

        } else {
            node.setPrev(tail);
            tail.setNext(node);
            tail = node;
        }
    }

    // Add a new node to the head of the linked list; this should have an O(1) implementation
    public void insertAtHead(T data)
    {
        Node node = new Node(data);
        if (head == null) {
            head = node;

            if (tail == null)
                tail = node;

        } else {
            node.setNext(head);
            head.setPrev(node);
            head = node;
        }
    }


    // Remove node from the head of this list and return the value of the removed node; should run in O(1) time
    public T removeFromHead()
    {
        Node node = head;
        if (head != null) {
            Node next = head.getNext();

            next.setPrev(null);
            head = next;
        }

        return node != null ? (T)node.data() : null;
    }

    // Remove node from the tail head of this list and return the removed node; should run in O(1) time
    public Node removeFromTail()
    {
        Node node = tail;
        if (tail != null) {
            //Node prev = tail.prev;
            tail.setPrev(tail.getPrev().getNext());
            //prev.next = null;
            //tail = prev;
        }

        return node;
    }

    // Remove a node at a given index and return the removed node; this implementation should run in linear time
    public Node removeAt(int index) {
        Node node = head;

        for(int i = 0; i < index && node.getNext() != null; i++)
            node = node.getNext();

        remove(node);

        return node;
    }

    // Remove the specified node from the linked list and return the data of the removed node; should run in O(1) time
    public T remove(Node node) {
        if (node.getPrev() != null)
            node.getPrev().setNext(node.getNext());

        if (node.getNext() != null)
            node.getNext().setPrev(node.getPrev());

        return (T)node.data();
    }

    // Remove the first node with the specified value in the linked list; linear time complexity
    public boolean remove(T data) {
        Node node = head;

        while(node.data() != data)
            node = node.getNext();

        return remove(node) == data;
    }

    // Find the index of the first node in the linked list that has the specified value
    public int indexOf(T data) {
        Node node = head;

        int index = 0;
        while(node.data() != data) {
            if (node.getNext() == null)
                return -1;

            node = node.getNext();
            index++;
        }

        return index;
    }

}
