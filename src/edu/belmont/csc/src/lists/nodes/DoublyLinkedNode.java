package edu.belmont.csc.src.lists.nodes;

public class DoublyLinkedNode<T> {
    private T data;
    private DoublyLinkedNode<T> next;
    private DoublyLinkedNode<T> prev;

    // constructor
    public DoublyLinkedNode(T data, DoublyLinkedNode<T> next, DoublyLinkedNode<T> prev)
    {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }

    public DoublyLinkedNode(T data)
    {
        this(data, null, null);
    }

    public DoublyLinkedNode<T> getNext()
    {
        return next;
    }

    public void setNext(DoublyLinkedNode next)
    {
        this.next = next;
    }

    public DoublyLinkedNode<T> getPrev()
    {
        return prev;
    }

    public void setPrev(DoublyLinkedNode prev)
    {
        this.prev = prev;
    }

    public T data()
    {
        return data;
    }
}