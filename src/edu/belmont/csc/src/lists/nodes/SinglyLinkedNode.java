package edu.belmont.csc.src.lists.nodes;

public class SinglyLinkedNode<T> {
    private T data;
    private SinglyLinkedNode<T> next;

    // constructor
    public SinglyLinkedNode(T data, SinglyLinkedNode<T> next)
    {
        this.data = data;
        this.next = next;
    }

    public SinglyLinkedNode(T data)
    {
        this(data, null);
    }

    public SinglyLinkedNode<T> getNext()
    {
        return next;
    }

    public void setNext(SinglyLinkedNode next)
    {
        this.next = next;
    }

    public T data()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }
}