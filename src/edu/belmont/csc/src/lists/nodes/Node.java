package edu.belmont.csc.src.lists.nodes;

public class Node<T> {
    private T data;
    private Node<T> next;
    private Node<T> prev;

    // constructor
    public Node(T data, Node<T> next, Node<T> prev)
    {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }

    public Node(T data)
    {
        this(data, null, null);
    }

    public Node<T> getNext()
    {
        return next;
    }

    public void setNext(Node next)
    {
        this.next = next;
    }

    public Node<T> getPrev()
    {
        return prev;
    }

    public void setPrev(Node prev)
    {
        this.prev = prev;
    }

    public T data()
    {
        return data;
    }
}