package edu.belmont.csc.src.lists;

public class DoublyLinkedList<T> {
    public Node head = null;
    public Node tail = null;

    public class Node<T> {
        public T data;
        public Node<T> next;
        public Node<T> prev;

        // constructor
        public Node(T data, Node<T> next, Node<T> prev)
        {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }

        public Node(T data)
        {
            this(data, null, null);
        }
    }

    // END OF TODO


    // Empty the linked list; this should take O(n) time
    public void clear()
    {
        if (isEmpty())
            return;

        Node node = tail;
        tail = null;

        while(node.prev != null) {
            node = node.prev;
            node.next = null;
        }

        head = null;
    }


    // Check if the linked list if empty
    public boolean isEmpty() {
        return head == null && tail == null;
    }

    // Append a node to the tail of the linked list; this should have an O(1) implementation
    public void append(T data)
    {
        Node node = new Node(data);

        if (tail == null) {
            tail = node;

            if (head == null)
                head = node;

        } else {
            node.prev = tail;
            tail.next = node;
            tail = node;
        }
    }

    // Add a new node to the head of the linked list; this should have an O(1) implementation
    public void insertAtHead(T data)
    {
        Node node = new Node(data);
        if (head == null) {
            head = node;

            if (tail == null)
                tail = node;

        } else {
            node.next = head;
            head.prev = node;
            head = node;
        }
    }


    // Remove node from the head of this list and return the value of the removed node; should run in O(1) time
    public T removeFromHead()
    {
        Node node = head;
        if (head != null) {
            Node next = head.next;

            next.prev = null;
            head = next;
        }

        return node != null ? (T)node.data : null;
    }

    // Remove node from the tail head of this list and return the removed node; should run in O(1) time
    public Node removeFromTail()
    {
        Node node = tail;

        if (tail.prev != null) {
            Node prev = tail.prev;

            prev.next = null;
            tail = prev;
        } else
            clear();

        return node;
    }

    // Remove a node at a given index and return the removed node; this implementation should run in linear time
    public Node removeAt(int index) {
        Node node = head;

        for(int i = 0; i < index && node.next != null; i++)
            node = node.next;

        remove(node);

        return node;
    }

    // Remove the specified node from the linked list and return the data of the removed node; should run in O(1) time
    public T remove(Node node) {
        if (node.prev != null)
            node.prev.next = node.next;

        if (node.next != null)
            node.next.prev = node.prev;

        return (T)node.data;
    }

    // Remove the first node with the specified value in the linked list; linear time complexity
    public boolean remove(T data) {
        Node node = head;

        while(node.data != data)
            node = node.next;

        return remove(node) == data;
    }

    // Find the index of the first node in the linked list that has the specified value
    public int indexOf(T data) {
        Node node = head;

        int index = 0;
        while(node.data != data) {
            if (node.next == null)
                return -1;

            node = node.next;
            index++;
        }

        return index;
    }

}