package edu.belmont.csc.src.trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BinarySearchTree {
    public static class Node {
        public int value;
        public Node left;
        public Node right;
        public int height;
        public int bf;

        public Node(int value)
        {
            this.value = value;
            this.left = null;
            this.right = null;
            this.height = 0;
            this.bf = 0;
        }
    }

    public Node root;
    private int nodeCount = 0;  // # of nodes in the tree.

    public BinarySearchTree() {}
    public BinarySearchTree(int root) {
        insert(root);
    }

    public void setRoot(Node node) {
        if (root == null)
            nodeCount++;

        root = node;
    }
    public void setRoot(int value) {
        setRoot(new Node(value));
    }
    public Node getRoot() {
        return root;
    }

    public int size() {
        return nodeCount;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Return whether a value exists in the tree
     * @param value
     * @return
     */
    public boolean contains(int value) {
        return contains(root, value);
    }

    /**
     * A recursive helper method to check if a value exists in the tree
     * @param node - current node
     * @param value - value to search for
     * @return true if found and false otherwise
     */
    private boolean contains(Node node, int value) {
        if (node == null)
            return false;

        if (node.value == value)
            return true;

        if (value < node.value && node.left != null)
            return contains(node.left, value);

        if (value > node.value && node.right != null)
            return contains(node.right, value);

        return false;
    }

    /**
     * Insert a value to the AVL tree. Duplicates are not allowed
     * @param value - value to be added
     * @return true if insertion is successful and false otherwise
     */
    public boolean insert(int value) {
        if (root == null) {
            root = new Node(value);
            nodeCount++;
        }

        if (!contains(root, value)) {
            root = insert(root, value);
            nodeCount++;
            return true;
        }
        return false;
    }

    /**
     * Recursive helper method to insert the given value to the tree while maintaining the BST invariant
     * @param node - root node of current subtree
     * @param value - value to be inserted
     * @return root node of current balanced subtree
     */
    private Node insert(Node node, int value) {
        if(node == null)
            return new Node(value);

        if (value < node.value)
            node.left = insert(node.left, value);
        else
            node.right = insert(node.right, value);

        update(node);
        return balance(node);
    }


    /**
     * Update a node's internal data when modified (hint: needed during insertion and removal)
     * @param node - node to be updated
     */
    private void update(Node node) {
        int leftHeight = height(node.left);
        int rightHeight = height(node.right);

        node.height = 1 + Math.max(leftHeight, rightHeight);
        node.bf = rightHeight - leftHeight;
    }

    /**
     * Get the height of a given tree
     * @param node - root of the tree
     * @return height of the tree (note that a tree with only one node is considered of height 0)
     */
    private int height(Node node) {
        return node == null ? -1 : node.height;
    }


    /**
     * Re-balance a node if its bf is +2 or -2 (hint: needed during insertion and removal)
     * @param node - root of current subtree
     * @return root of balanced subtree
     */
    private Node balance(Node node) {
        update(node);

        if (node.bf > 1) {
            if (node.right.bf <= 0)
                node.right = rightRotate(node.right);
            node = leftRotate(node);

        } else if (node.bf < -1) {
            if (node.left.bf >= 0)
                node.left = leftRotate(node.left);
            node = rightRotate(node);

        }

        return node;
    }


    /**
     * Performs left rotation on the specified node
     * @param node - root of subtree
     * @return new root of subtree
     */
    private Node leftRotate(Node node) {
        Node right = node.right;
        node.right = right.left;
        right.left = node;

        update(node);
        update(right);

        return right;
    }

    /**
     * Performs right rotation on the specified node
     * @param node - root of subtree
     * @return new root of subtree
     */
    private Node rightRotate(Node node) {
        Node left = node.left;
        node.left = left.right;
        left.right = node;

        update(node);
        update(left);

        return left;
    }

    /**
     * Find leftmost child of a node
     * @param node - root of current subtree
     * @return leftmost child node
     */
    private Node leftMostChild(Node node) {
        Node current = node;

        while (current.left != null)
            current = current.left;

        return current;
    }

    // Remove a value from this binary tree if it exists, O(log(n))
    /**
     * Removes the specified value from the tree
     * @param element - element to be removed
     * @return true if successfully removed and false otherwise
     */
    public boolean remove(int element) {
        if (contains(element)) {
            root = remove(root, element);
            nodeCount = !contains(element) ? nodeCount - 1 : nodeCount;
            return !contains(element);
        }

        return false;
    }

    /**
     * A recursive helper method to remove the element from the specified tree
     * @param node - root of current subtree
     * @param element - element to be removed
     * @return root of balanced subtree
     */
    private Node remove(Node node, int element) {
        update(node);
        if (node == null)
            return node;
        else if (node.value > element)
            node.left = remove(node.left, element);
        else if (node.value < element)
            node.right = remove(node.right, element);
        else {
            if (node.left == null || node.right == null)
                node = node.left == null ? node.right : node.left;
            else {
                Node leftMost = leftMostChild(node.right);
                node.value = leftMost.value;
                node.right = remove(node.right, node.value);
            }
        }

        if (node != null)
            node = balance(node);

        return node;
    }


    /**
     * A recursive method to check if a given tree is a valid binary search tree
     * @param root - root of tree
     * @return true if valid and false otherwise
     */
    public boolean isValid(Node root) {
        return isValid(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public boolean isValid(Node node, int min, int max) {
        if (node == null)
            return true;

        if (node.right != null && node.value >= node.right.value)
            return false;

        if (node.left != null && node.value <= node.left.value)
            return false;

        if (node.value < min || node.value > max)
            return false;

        return (isValid(node.left, min, node.value-1) && isValid(node.right, node.value+1, max));
    }

    public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> result = new ArrayList<>();

        if (root==null)
            return result;

        LinkedList<Node> nodes = new LinkedList<>();
        LinkedList<Integer> levels = new LinkedList<>();

        nodes.offer(root);
        levels.offer(1);

        while (!nodes.isEmpty()) {
            Node node = nodes.poll();
            int level = levels.poll();

            List<Integer> l = null;
            if (result.size() < level){
                l = new ArrayList<>();
                result.add(l);
            } else
                l = result.get(level-1);

            l.add(node.value);

            if (node.left != null) {
                nodes.offer(node.left);
                levels.offer(level+1);
            }

            if (node.right != null) {
                nodes.offer(node.right);
                levels.offer(level+1);
            }
        }

        return result;
    }

    // Driver method
    public static void main(String[] args)
    {
        int[] arr = { 1, 5, 4, 3, 2, 1, 7, 11, 13, 12, 17, 14, 13, 10, 9, 15 };
        int[] arr1 = { 1, 2, 3, 4, 5, 15, 17 };
        int[] arr2 = { 8, 5, 11, 2, 3, 9, -3 };

        BinarySearchTree tree = new BinarySearchTree();

        for(int i : arr)
            tree.insert(i);

        printTree(tree);
        while (!tree.isEmpty()) {
            System.out.println();
            System.out.println("\nRemoving: " + tree.root.value + "...");
            tree.remove(tree.root.value);
            printTree(tree);
            System.out.println("\nSize: " + tree.size());
            System.out.println("\n=========================================");
        }

        // TODO: add your own test cases. For full credit, your tests must check that all methods are correctly implemented

    }

    public static void printTree(BinarySearchTree tree) {
        BinaryTreePrinter printer = new BinaryTreePrinter(tree);
        printer.print(System.out);
    }

    public static void printTree(Node root) {
        BinaryTreePrinter printer = new BinaryTreePrinter(root);
        printer.print(System.out);
    }
}