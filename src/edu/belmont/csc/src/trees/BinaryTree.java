package edu.belmont.csc.src.trees;


import java.util.Arrays;

public class BinaryTree {
    public static class Node{
        int value;
        Node left;
        Node right;
        int height;
        int balanceFactor;

        public Node(int value) {
            this.value = value;
            left = null;
            right = null;
            height = 0;
            balanceFactor = 0;

            //update(this);
        }
    }

    static Node update(Node n) {
        height(n);
        return n;
    }

    /**
     * Given two trees, check to see if they are identical (nodes with identical values and are positioned
     * the same)
     * @param n1 - the first tree
     * @param n2 - the second tree
     * @return true if identical and false otherwise
     */
    public static boolean isIdentical(Node n1, Node n2) {
        boolean identical = true;

        if (n1.value != n2.value)
            return false;
        else {
            if (n1.left == null && n2.left == null)
                return true;
            else if (n1.left != null && n2.left != null)
                identical = identical && isIdentical(n1.left, n2.left);
            else return false;

            if (n1.right == null && n2.right == null)
                return true;
            else if (n1.right != null && n2.right != null)
                identical = identical && isIdentical(n1.right, n2.right);
            else return false;
        }

        return identical;
    }

    /**
     * Get the height of a given tree
     * @param root - root of the tree
     * @return height of the tree (note that a tree with only one node is considered of height 0)
     */
    public static int height(Node root) {
        int rightHeight = 0;
        int leftHeight = 0;

        if (root.left == null && root.right == null)
            root.height = 0;
        if (root.right != null)
            rightHeight += 1 + height(root.right);
        if (root.left != null)
            leftHeight += 1 + height(root.left);

        root.height = leftHeight >= rightHeight ? leftHeight : rightHeight;
        root.balanceFactor = rightHeight - leftHeight;

        System.out.println("Node Value: " + root.value + ", Right Height: " + rightHeight + ", Left Height: " + leftHeight + ", Balance Factor: " + root.balanceFactor);

        return root.height;
    }

    /**
     * Check if given binary tree is a height balanced tree, i.e., |balance factor| <= 1
     * @param root - root of tree
     * @return true if height balanced and false otherwise
     */
    public static boolean isHeightBalanced(Node root)
    {
        if (root.balanceFactor == 0)
            height(root);
        int bf = root.balanceFactor;
        return (bf <= 0 && bf >= -1) || (bf >= 0 && bf <= 1);
        //return false;
    }

    // driver function
    public static void main(String[] args) {
        // construct first tree
        Node x = new Node(15);
        x.left = new Node(10);
        x.right = new Node(20);
        x.left.left = new Node(8);
        x.left.right = new Node(12);
        x.right.left = new Node(16);
        //x.right.right = new Node(25);

        // construct second tree
        Node y = new Node(15);
        y.left = new Node(10);
        y.right = new Node(20);
        y.left.left = new Node(8);
        y.left.right = new Node(12);
        y.right.left = new Node(16);
        y.right.right = new Node(25);

        if (isIdentical(x, y)) {
            System.out.println("Given binary Trees are identical");
        } else {
            System.out.println("Given binary Trees are not identical");
        }


        boolean identical = isIdentical(x,y);

        System.out.println("The height of the second binary tree is " + height(y));

        System.out.print("Tree height balanced: ");
        if (isHeightBalanced(x)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}