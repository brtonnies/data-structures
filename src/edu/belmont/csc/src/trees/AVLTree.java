package edu.belmont.csc.src.trees;

public class AVLTree {

    public class Node {
        int value;
        int height;
        int bf;
        Node left;
        Node right;

        Node(int value) {
            this.value = value;
            this.left = null;
            this.right = null;
            this.height = 0;
            this.bf = 0;
        }
    }

    public Node root;
    public int nodeCount;
    public AVLTree() {}

    public int height() {
        return root == null ? -1 : root.height;
    }

    /**
     * Return whether a value exists in the tree
     * @param value
     * @return
     */
    public boolean contains(int value) {
        return contains(root, value);
    }

    /**
     * A recursive helper method to check if a value exists in the tree
     * @param node - current node
     * @param value - value to search for
     * @return true if found and false otherwise
     */
    private boolean contains(Node node, int value) {
        if (node == null)
            return false;

        if (node.value == value)
            return true;

        if (value < node.value && node.left != null)
            return contains(node.left, value);

        if (value > node.value && node.right != null)
            return contains(node.right, value);

        return false;
    }

    /**
     * Insert a value to the AVL tree. Duplicates are not allowed
     * @param value - value to be added
     * @return true if insertion is successful and false otherwise
     */
    public boolean insert(int value) {
        if (root == null) {
            root = new Node(value);
            nodeCount++;
        }

        if (!contains(root, value)) {
            root = insert(root, value);
            nodeCount++;
            return true;
        }
        return false;
    }

    /**
     * Recursive helper method to insert the given value to the tree while maintaining the BST invariant
     * @param node - root node of current subtree
     * @param value - value to be inserted
     * @return root node of current balanced subtree
     */
    private Node insert(Node node, int value) {
        if (node == null) {
            return new Node(value);
        } else if (node.value > value) {
            node.left = insert(node.left, value);
        } else if (node.value < value) {
            node.right = insert(node.right, value);
        } else {
            throw new RuntimeException("duplicate Key!");
        }
        return rebalance(node);
    }

    // Remove a value from this binary tree if it exists, O(log(n))

    /**
     * Removes the specified value from the tree
     * @param element - element to be removed
     * @return true if successfully removed and false otherwise
     */
    public boolean remove(int element) {
        if (contains(element)) {
            root = remove(root, element);
            nodeCount = !contains(element) ? nodeCount - 1 : nodeCount;
            return !contains(element);
        }

        return false;
    }

    /**
     * A recursive helper method to remove the element from the specified tree
     * @param node - root of current subtree
     * @param element - element to be removed
     * @return root of balanced subtree
     */
    private Node remove(Node node, int element) {
        if (node == null) {
            return node;
        } else if (node.value > element) {
            node.left = remove(node.left, element);
        } else if (node.value < element) {
            node.right = remove(node.right, element);
        } else {
            if (node.left == null || node.right == null) {
                node = (node.left == null) ? node.right : node.left;
            } else {
                Node mostLeftChild = leftmostChild(node.right);
                node.value = mostLeftChild.value;
                node.right = remove(node.right, node.value);
            }
        }
        if (node != null) {
            node = rebalance(node);
        }
        return node;
    }

    private Node leftmostChild(Node node) {
        Node current = node;
        while (current.left != null)
            current = current.left;

        return current;
    }

    private Node rebalance(Node node) {
        update(node);
//        int balance = getBalance(node);
//        if (balance > 1) {
        if (node.bf > 1) {
//            if (height(node.right.right) > height(node.right.left)) {
            if (node.right.bf > 0) {
                node = leftRotate(node);
            } else {
                node.right = rightRotate(node.right);
                node = leftRotate(node);
            }
//        } else if (balance < -1) {
        } else if (node.bf < -1) {
//            if (height(node.left.left) > height(node.left.right)) {
            if (node.left.bf < 0) {
                node = rightRotate(node);
            } else {
                node.left = leftRotate(node.left);
                node = rightRotate(node);
            }
        }
        return node;
    }

    private Node rightRotate(Node node) {
        System.out.println("RIGHT ROTATION: " + node.value);
        Node left = node.left;
//        Node z = left.right;
        node.left = left.right;
        left.right = node;
//        node.left = z;
        update(node);
        update(left);
        return left;
    }

    private Node leftRotate(Node node) {
        System.out.println("LEFT ROTATION: " + node.value);
        Node right = node.right;
//        Node z = right.left;
        node.right = right.left;
        right.left = node;
//        node.right = z;
        update(node);
        update(right);
        return right;
    }

//    private void update(Node n) {
//        n.height = 1 + Math.max(height(n.left), height(n.right));
//    }

    /**
     * Update a node's internal data when modified (hint: needed during insertion and removal)
     * @param node - node to be updated
     */
    private void update(Node node) {
        int leftHeight = height(node.left);
        int rightHeight = height(node.right);

        node.height = 1 + Math.max(leftHeight, rightHeight);
        node.bf = rightHeight - leftHeight;
    }

    private int height(Node node) {
        return node == null ? -1 : node.height;
    }

//    public int getBalance(Node n) {
//        return (n == null) ? 0 : height(n.right) - height(n.left);
//    }

    public static void printTree(AVLTree tree) {
        AVLTreePrinter printer = new AVLTreePrinter(tree);
        printer.print(System.out);
    }

    public static void main(String[] args) {
//        int[] arr2 = { 8, 5, 11, 2, 3, 9, -3 };
        int[] arr2 = { 6,2,10,8,12 };
        AVLTree tree = new AVLTree();
        for(int i : arr2)
            tree.insert(i);

        printTree(tree);

        tree.insert(7);
        printTree(tree);

        System.out.println();
//        System.out.println("\nRemoving: " + tree.root.value + "...");
//        tree.remove(tree.root.value);
//        printTree(tree);
////        System.out.println("\nSize: " + tree.size());
//        System.out.println("\n=========================================");
//
//        System.out.println("Removing: " + tree.root.value + "...");
//        tree.remove(tree.root.value);
//        printTree(tree);
////        System.out.println("\nSize: " + tree.size());
//        System.out.println("\n=========================================");
    }
}