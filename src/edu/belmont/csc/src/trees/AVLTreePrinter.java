package edu.belmont.csc.src.trees;

import java.io.PrintStream;

public class AVLTreePrinter {
    public static String RPOINTER="└──";
    public static String LPOINTER="├──\u24c1 ";


    private AVLTree tree;

    public AVLTreePrinter(AVLTree tree) {
        this.tree = tree;
    }

    private String traversePreOrder(AVLTree.Node root) {

        if (root == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(root.value);

        String pointerLeft = (root.right != null) ? LPOINTER : RPOINTER+"\u24c1 ";

        traverseNodes(sb, "", pointerLeft, root.left, root.right != null);
        traverseNodes(sb, "", RPOINTER+"\u24c7 ", root.right, false);

        return sb.toString();
    }

    private void traverseNodes(StringBuilder sb, String padding, String pointer, AVLTree.Node node,
                               boolean hasRightSibling) {

        if (node != null) {

            sb.append("\n");
            sb.append(padding);
            sb.append(pointer);
            sb.append(node.value);

            StringBuilder paddingBuilder = new StringBuilder(padding);
            if (hasRightSibling) {
                paddingBuilder.append("│  ");
            } else {
                paddingBuilder.append("   ");
            }

            String paddingForBoth = paddingBuilder.toString();
            String pointerLeft = (node.right != null) ? LPOINTER : RPOINTER+"\u24c1 ";

            traverseNodes(sb, paddingForBoth, pointerLeft, node.left, node.right != null);
            traverseNodes(sb, paddingForBoth, RPOINTER+("\u24c7 "), node.right, false);

        }

    }

    public void print(PrintStream os) {
        os.print(traversePreOrder(tree.root));
    }

}