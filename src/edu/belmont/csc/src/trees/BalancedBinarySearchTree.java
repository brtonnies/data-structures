package edu.belmont.csc.src.trees;

import edu.belmont.csc.src.search.Node;

/**
 * BALANCED BINARY SEARCH TREES
 *
 * AVL Tree:
 * -- The first type of BBST to be discovered
 * -- Named after its inventors
 * Information at Each Node
 * -- Value
 * -- Height
 * -- Left/Right Children
 * -- Balance Factor: BF = right.height - left.height
 *
 * Insertion:
 *
 *
 * Right-Rotate
 * rightRotate(Q):
 *   P = Q.left
 *   Q.left = P.right
 *   P.right = Q
 *   return P
 *
 * leftRotate(P):
 *   Q = P.right
 *   P.right = Q.right
 *
 *
 * LeftLeft Case
 * LeftRight Case
 *
 * RightRight Case
 * RightLeft Case
 *
 *
 *
 *
 *
 */

public class BalancedBinarySearchTree {
    public class Node{
        int value;
        Node left = null;
        Node right = null;
        int height;
        int balanceFactor;

        public Node(int value) {
            this.value = value;
        }
    }

    Node root = null;



    boolean insert(int value) {
        root = insert(root, value);
        return true;
    }

    Node insert(Node n, int v) {
        if(n == null) return new Node(v);
        if (v < n.value)
            n.left = insert(n.left, v);
        else
            n.right = insert(n.right, v);

         update(n);
         return balance(n);
    }


    Node balance(Node n) {
        /* 4 cases to consider
            (1) LeftLeft Heavy   -- Right Rotate root
                -- BF < -1
            (2) LeftRight Heavy  -- Left Rotate left child of root, then Right Rotate root

            (3) RightRight Heavy -- Left Rotate root

            (4) RightLeft Heavy  -- Right Rotate right child of root, then Left Rotate root

        */
        return null;
    }

    void update(Node n) {

    }

    Node remove(Node n) {
        return null;
    }
}
