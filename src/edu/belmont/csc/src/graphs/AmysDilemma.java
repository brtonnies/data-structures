package edu.belmont.csc.src.graphs;

public class AmysDilemma {

public static int[] colorHelp(int h, int[][] paths) {
    int numColors = 4;
    int[] pencils = new int[h];

    int color = 1;
    for(int c = 1; c <= h; c++) {
        pencils[c-1] = color;
        for (int i = 0; i < paths.length; i++) {
            int h1 = paths[i][0];
            int h2 = paths[i][1];

            if (h1 == c && (pencils[h1-1] == pencils[h2-1] || pencils[h2-1] == 0))
                    pencils[h2 - 1] = color + 1;

            if (h2 == c && (pencils[h1-1] == pencils[h2-1] || pencils[h1-1] == 0))
                    pencils[h1 - 1] = color + 1;

        }

        color = (color + 1) > numColors ? (color + 1) % numColors : color + 1;
    }

    printPencils(pencils);
    return pencils;
}

public static void printPencils(int[] pencils) {
    System.out.print("[ ");
    for(int i = 0; i < pencils.length; i++)
        System.out.print(i < pencils.length - 1 ? pencils[i] + ", " : pencils[i]);

    System.out.print(" ]\n");
}


    public static void main(String[] args) {
        int[][] input1 = { {1,2}, {1,3}, {2,3} };
        int[][] input2 = { {1,2}, {2,3}, {3,4}, {4,1}, {1,3}, {2,4} };


        System.out.println("First Input: \n[ [1, 2], [1, 3], [2, 3] ]");
        System.out.println("Output: ");
        colorHelp(3, input1);
        System.out.println();
        System.out.println("Second Input: \n[ [1, 2], [2, 3], [3, 4], [4, 1], [1, 3], [2, 4] ]");
        System.out.println("Output: ");
        colorHelp(4, input2);

    }
}
