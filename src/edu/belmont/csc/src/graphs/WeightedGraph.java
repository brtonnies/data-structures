package edu.belmont.csc.src.graphs;


import java.util.HashMap;
import java.util.ArrayList;

public class WeightedGraph {
    static class Edge {
        char src;
        char dst;
        int weight;

        public Edge(char src, char dst, int weight) {
            this.src = src;
            this.dst = dst;
            this.weight = weight;
        }
    }


    int vertices;
//    LinkedList<Edge>[] adjList;
    HashMap<Character, ArrayList<Edge>> adjList = new HashMap<>();

    public WeightedGraph(int vertices) {
        this.vertices = vertices;
        adjList = new HashMap<>();

//        for (int i = 0; i < vertices; i++) {
//            adjList[i] = new LinkedList<>();
//        }
    }

    public void addVertex(char v) {
        adjList.put(v, new ArrayList<>());
    }

    public void addEgde(char src, char dst, int weight) {
        if (existsEdge(src, dst, weight))
            return;

        while (!adjList.containsKey(src) || !adjList.containsKey(dst))
            addVertex(adjList.containsKey(src) ? dst : src);

        if (adjList.containsKey(src) && adjList.containsKey(dst)) {
            ArrayList<Edge> fromList = adjList.get(src);
            ArrayList<Edge> toList = adjList.get(dst);
            fromList.add(new Edge(dst, src, weight));
            toList.add(new Edge(src, dst, weight));
            adjList.replace(src, fromList);
            adjList.replace(dst, toList);
        }
    }

    public boolean existsEdge(char src, char dst, int weight) {

        for(char k : adjList.keySet())
            if (adjList.containsKey(k))
                for (Edge e : adjList.get(k))
                    if (((e.src == src && e.dst == dst) || (e.dst == src && e.src == dst)) && e.weight == weight)
                        return true;

        return false;
    }

    public void printGraph() {
        for (char k : adjList.keySet()) {
            for (Edge edge : adjList.get(k))
                System.out.println("[ " + edge.src + ", " + edge.dst + " ]: " +  edge.weight);
        }
    }

    public int vertices() {
        return adjList.keySet().size();
    }

    public static void main(String[] args) {
        int vertices = 6;
        WeightedGraph graph = new WeightedGraph(vertices);
        graph.addEgde('a', 'b', 4);
        graph.addEgde('a', 'c', 3);
        graph.addEgde('b', 'd', 2);
        graph.addEgde('b', 'c', 5);
        graph.addEgde('c', 'd', 7);
        graph.addEgde('d', 'e', 2);
        graph.addEgde('e', 'a', 4);
        graph.addEgde('e', 'b', 4);
        graph.addEgde('e', 'f', 6);
        graph.printGraph();
    }
}