package edu.belmont.csc.src.graphs;

import edu.belmont.csc.src.queues.Queue;

import java.util.*;


public class GraphTraversals {

    public static ArrayList<Character> bfsTraversal(UnweightedGraph graph, char v) {
        Queue<Character> q = new Queue<>();
        Set<Character> visited = new HashSet<>();
        ArrayList<Character> vertices = new ArrayList<>();

        q.enqueue(v);
        while (!q.isEmpty()) {
            char n = q.dequeue();
            if (!visited.contains(n)) {
                vertices.add(n);
                visited.add(n);
                for (char k : graph.adjList.get(n)) {
                    if (!visited.contains(k))
                        q.enqueue(k);
                }
            }
        }
        return vertices;
    }

    public static Map<Character, Integer> shortestPath(UnweightedGraph graph, char v) {
        ArrayList<Character> vList = bfsTraversal(graph, v);
        Queue<Character> q = new Queue<>();
        Set<Character> visited = new HashSet<>();
        Map<Character, Integer> distances = new HashMap<>();

        visited.add(v);
        distances.put(v, 0);
        vList.remove(0);
        q.enqueue(v);
        for(char target : vList) {
            distances.putIfAbsent(target, 0);
            while (!q.isEmpty()) {
                char n = q.dequeue();
                for (char k : graph.adjList.get(n)) {
                    distances.putIfAbsent(k, 0);
                    if (!visited.contains(k)) {
                        visited.add(k);
                        q.enqueue(k);
                        distances.replace(k, distances.get(n) + 1);
                    }
                }
            }

        }

        return distances;
    }

    public static void main(String[] args) {
        char[] vertices = { 'A', 'B', 'C', 'D', 'E' };
        UnweightedGraph graph = new UnweightedGraph();
        graph.addEdge('A', 'B');
        graph.addEdge('A', 'C');
        //graph.addEdge('C', 'B');
        graph.addEdge('D', 'C');
        graph.addEdge('C', 'E');
        //graph.addEdge('A', 'E');
        graph.addEdge('D', 'E');

//        graph.printGraph();

        System.out.println("\nBFS Traversals:");

        for (char v : vertices) {
            System.out.print("[" + v + "]: ");
            ArrayList<Character> bfs = bfsTraversal(graph, v);
            for (char u : bfs)
                System.out.print(bfs.indexOf(u) < bfs.size() - 1 ? u + " -> " : u);
            System.out.println();
        }

        System.out.println("\nShortest Paths: ");

        for (char v : vertices) {
            System.out.println("From Vertex: " + v + ":");
            for (char key : shortestPath(graph, v).keySet())
                System.out.println(key + ": " + shortestPath(graph, v).get(key));

            System.out.println();
        }



    }
}
