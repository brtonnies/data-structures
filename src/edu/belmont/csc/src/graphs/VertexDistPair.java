package edu.belmont.csc.src.graphs;

public class VertexDistPair implements Comparable<VertexDistPair> {
    char key;
    int value;

    public VertexDistPair(char key, int value) {
        this.key = key;
        this.value = value;
    }

    public int compareTo(VertexDistPair p2) {
        return this.value - p2.value;
    }
}
