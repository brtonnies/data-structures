package edu.belmont.csc.src.graphs;



import java.util.*;

public class Programming5 {


    public static int[] dijkstra(WeightedGraph g, char source) {
        //TODO: implement this algorithm to return the shortest paths distances from source to every vertex in g
        // You may assume g contains no edges with negative weights
        // TODO: print out the shortest paths from source to every vertex in g in console

        boolean[] s = new boolean[g.vertices()];
        ArrayList<Character> vertices = new ArrayList<>(g.vertices());
        int[] d = new int[g.vertices()];
        char[] p = new char[g.vertices()];

        int i = 0;
        for(char v : g.adjList.keySet()) {
            s[i] = false;
            vertices.add(v);
            d[i] = Integer.MAX_VALUE;
            p[i] = source;
            i++;
        }

        PriorityQueue<VertexDistPair> pq = new PriorityQueue<>(vertices.size(), new Comparator<VertexDistPair>() {
            @Override
            public int compare(VertexDistPair p1, VertexDistPair p2) {
                return p1.value - p2.value;
            }
        });

        d[0] = 0;

        VertexDistPair curr = new VertexDistPair(source, 0);;
        pq.add(curr);

        while (!pq.isEmpty()) {
            curr = pq.poll();
            ArrayList<WeightedGraph.Edge> adjacent = g.adjList.get(curr.key);

            if (!s[vertices.indexOf(curr.key)]) {
                s[vertices.indexOf(curr.key)] = true;

                for(i = 0; i < adjacent.size(); i++) {
                    WeightedGraph.Edge edge = adjacent.get(i);
                    char dst = edge.dst;
                    char src = edge.src;

                    if (!s[vertices.indexOf(src)]) {
                        int newWeight = d[vertices.indexOf(dst)] + edge.weight;
                        int oldWeight = d[vertices.indexOf(src)];
                        p[vertices.indexOf(src)] = dst;

                        if (newWeight < oldWeight) {
                            d[vertices.indexOf(src)] = newWeight;
                            pq.add(new VertexDistPair(src, newWeight));
                        }
                    }
                }
            }
        }

        printArray(p);
        System.out.println("Dijkstra's Shortest Paths From Source '" + source + "'");
        for (i = 0; i < vertices.size(); i++) {
            System.out.println("\t \u2b62 To vertex: '" + vertices.get(i) + "': " + d[i]);
        }

        return d;
    }

    public static int smallestIdx(int[] arr) {
        int smallest = 0;
        for(int i = 0; i < arr.length; i++)
            smallest = arr[i] < smallest ? i : smallest;

        return smallest;
    }

    public static void printArray(int[] arr) {
        System.out.print("[ ");
        for(int i =0; i < arr.length; i++)
            System.out.print(i < arr.length - 1 ? arr[i] + ", " : arr[i]);

        System.out.println(" ]");
    }

    public static void printArray(char[] arr) {
        System.out.print("[ ");
        for(int i =0; i < arr.length; i++)
            System.out.print(i < arr.length - 1 ? arr[i] + ", " : arr[i]);

        System.out.println(" ]");
    }

    private static void visit(char v) {
        System.out.println(v);
    }

    public static void dfs(char v, Set<Character> visited,
                           HashMap<Character, ArrayList<Character>> map) {
        if (visited.contains(v)) return;

        visit(v);
        visited.add(v);

        for (char n: map.get(v))
            if (!visited.contains(n))
                dfs(n, visited, map);
    }

    public static void dfs(char v, Set<Character> visited, Stack<Character> topStack,
                           HashMap<Character, ArrayList<Character>> map) {
        if (visited.contains(v))
            return;

        visited.add(v);

        for (char n: map.get(v))
            if (!visited.contains(n)) {
                topStack.push(n);
                dfs(n, visited, topStack, map);
            }
    }

    public static void dfsTraversal(HashMap<Character, ArrayList<Character>> map) {
        Set<Character> visited = new HashSet<>();

        for (Character v: map.keySet())
            dfs(v, visited, map);
    }

    public static Stack<Character> dfsTraversal(Set<Character> visited, Stack<Character> topStack,
                                                HashMap<Character, ArrayList<Character>> map) {

        for (Character v: map.keySet()) {
            if (!topStack.contains(v)) topStack.push(v);
            dfs(v, visited, topStack, map);
        }

        return topStack;
    }


    public static char[] topsort(UnweightedGraph g) {
        //TODO: implement this algorithm to return a valid topological ordering of the vertices
        // You may assume that g is a DAG

        HashMap<Character, ArrayList<Character>> map = g.adjList;
        char[] topOrder = new char[map.size()];
        Set<Character> visited = new HashSet<>();
        Stack<Character> topStack = new Stack<>();

        topStack = dfsTraversal(visited, topStack, map);

        int i = topOrder.length - 1;
        while (!topStack.empty()) {
            topOrder[i] = topStack.pop();
            i--;
        }


        return topOrder;
    }


    public static void main(String[] args) {
        int vertices = 6;
        WeightedGraph graph = new WeightedGraph(vertices);
        graph.addEgde('a', 'b', 4);
        graph.addEgde('a', 'c', 3);
        graph.addEgde('b', 'd', 2);
        graph.addEgde('b', 'c', 5);
        graph.addEgde('c', 'd', 7);
        graph.addEgde('d', 'e', 2);
        graph.addEgde('e', 'a', 4);
        graph.addEgde('e', 'f', 6);

        int[] distances = dijkstra(graph, 'a');
        System.out.println();
        printArray(distances);


        UnweightedGraph graph2 = new UnweightedGraph();
        graph2.addEdge('a', 'b');
        graph2.addEdge('a', 'c');
        graph2.addEdge('b', 'd');
//        graph2.addEdge('c', 'a');
        graph2.addEdge('c', 'd');
//        graph2.addEdge('d', 'd');

        //graph.printGraph();

        System.out.println("\nTopological Sort:");
        char[] top = topsort(graph2);

        for (int i = 0; i < top.length; i++)
            System.out.print(i < top.length - 1 ? top[i] + ", " : top[i]);
    }

}
