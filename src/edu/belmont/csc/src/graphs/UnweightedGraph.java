package edu.belmont.csc.src.graphs;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is a class that uses an adjacency list to implement an unweighted and undirected graph
 * Nodes in this graph are of type char
 * Your task is to use any existing data structure to complete the class below.
 */
public class UnweightedGraph {
    HashMap<Character, ArrayList<Character>> adjList;

    public UnweightedGraph() {
        adjList = new HashMap<>();
    }

    public void addVertex(char v) {
        adjList.put(v, new ArrayList<>());
    }

    public void removeVertex(char v) {
        for(char u : adjList.get(v))
            removeEdge(u, v);

        adjList.remove(v);
    }

    public void addEdge(char from, char to) {
        if (existsEdge(from, to))
            return;

        while (!adjList.containsKey(from) || !adjList.containsKey(to))
            addVertex(adjList.containsKey(from) ? to : from);

        if (adjList.containsKey(from) && adjList.containsKey(to)) {
            ArrayList<Character> fromList = adjList.get(from);
            ArrayList<Character> toList = adjList.get(to);
            fromList.add(to);
            toList.add(from);
            adjList.replace(from, fromList);
            adjList.replace(to, toList);
        }
    }

    public void removeEdge(char from, char to) {
        if (existsEdge(from, to))
            adjList.get(from).remove(to);
    }

    public boolean existsEdge(char from, char to) {

        return adjList.containsKey(from) && adjList.get(from).contains(to);
    }

    public void printGraph() {
        for(char v : adjList.keySet()) {
            System.out.print("\n[" + v + "] -> ");
            for(char u : adjList.get(v))
                System.out.print(adjList.get(v).indexOf(u) < adjList.get(v).size() - 1 ? u + " -> " : u);
        }

    }

    public static void main(String[] args) {
        UnweightedGraph graph = new UnweightedGraph();
        graph.addEdge('A', 'B');
        graph.addEdge('A', 'C');
        graph.addEdge('C', 'B');
        graph.addEdge('D', 'C');
        graph.addEdge('C', 'E');
        graph.addEdge('A', 'E');

        graph.printGraph();
    }
}