package edu.belmont.csc.src.graphs;

import edu.belmont.csc.src.queues.Queue;

import java.util.*;

public class DirectedGraph {

    Map<Character, ArrayList<Character>> map;

    public DirectedGraph() {

        this.map = new HashMap<>();
    }

    public void addVertex(char v) {
        map.put(v, new ArrayList<>());
    }

    public void addEdge(char from, char to) {
        if (!map.containsKey(from)) {
            addVertex(from);
        }
        if (!map.containsKey(to)) addVertex(to);

        map.get(from).add(to);
    }


    private void visit(char v) {
        System.out.println(v);
    }

    public boolean hasCycle(char v, Set<Character> visited, Stack<Character> callstack) {
        if (callstack.contains(v)) return true;
        if (visited.contains(v)) return false;

        visit(v);
        visited.add(v);

        for (char n: map.get(v)) {
            if (!visited.contains(n)) {
                boolean cycleFound = hasCycle(n, visited, callstack);
                if (cycleFound)
                    return true;
            }
        }

        return false;
    }

    public boolean hasCycle() {
        Set<Character> visited = new HashSet<>();
        Stack<Character> callstack = new Stack<>();
//        boolean cycleFound = false;

        for (Character v: map.keySet()) {
            callstack.push(v);
            boolean cycleFound = hasCycle(v, visited, callstack);

            if (cycleFound)
                return true;
        }

        return false;
    }

    public void dfs(char v, Set<Character> visited) {
        if (visited.contains(v)) return;

        visit(v);
        visited.add(v);

        for (char n: map.get(v)) {
            if (!visited.contains(n)) {
                dfs(n, visited);
            }
        }
    }

//    public void dfs(char v, Set<Character> visited) {
//        if (visited.contains(v)) return;
//
//        visit(v);
//        visited.add(v);
//
//        for (char n: map.get(v)) {
//            if (!visited.contains(n)) {
//                dfs(n, visited);
//            }
//        }
//    }

    public void dfs(char v, Set<Character> visited, Stack<Character> topStack) {

        if (visited.contains(v)) return;

        //visit(v);
        visited.add(v);

        for (char n: map.get(v)) {
            if (!visited.contains(n)) {
                topStack.push(n);
                dfs(n, visited, topStack);
            }
        }

    }

    public void dfsTraversal() {
        Set<Character> visited = new HashSet<>();

        for (Character v: map.keySet()) {
            dfs(v, visited);
        }
    }

    public Stack<Character> dfsTraversal(Set<Character> visited, Stack<Character> topStack) {

        for (Character v: map.keySet()) {
            if (!topStack.contains(v)) topStack.push(v);
            dfs(v, visited, topStack);
        }

        return topStack;
    }

    public void printGraph() {
        for(char v : map.keySet()) {
            System.out.print("\n[" + v + "] -> ");
            for(char u : map.get(v))
                System.out.print(map.get(v).indexOf(u) < map.get(v).size() - 1 ? u + " -> " : u);
        }

    }

    /**
     * Topological sort algorithm that returns one topological ordering of the graph nodes
     * Hint: you can self-check by following your output ordering to see if the result meets all the order constraints
     * defined by the directed edges
     * @return an array of char's
     */
    public char[] topsort() {
        // TODO: Complete this method by modifying the dfsTraversal() method

        // initialize your variables
        char[] topOrder = new char[map.size()];
        Set<Character> visited = new HashSet<>();
        Stack<Character> topStack = new Stack<>();


        // call dfs (*hint*: you shouldn't have to modify the recursive dfs method!
        topStack = dfsTraversal(visited, topStack);

        int i = topOrder.length - 1;
        while (!topStack.empty()) {
            topOrder[i] = topStack.pop();
            i--;
        }


        return topOrder;
        // END OF TODO
    }

    public static void main(String[] args) {
        DirectedGraph graph = new DirectedGraph();
        graph.addEdge('0', '1');
        graph.addEdge('0', '2');
        graph.addEdge('1', '3');
//        graph.addEdge('2', '0');
        graph.addEdge('2', '3');
//        graph.addEdge('3', '3');

        graph.dfsTraversal();

        //graph.printGraph();

        System.out.println("\nTopological Sort:");
        char[] top = graph.topsort();

        for (int i = 0; i < top.length; i++)
            System.out.print(i < top.length - 1 ? top[i] + ", " : top[i]);

    }
}