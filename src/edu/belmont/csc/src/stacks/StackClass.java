package edu.belmont.csc.src.stacks;

import edu.belmont.csc.src.queues.Queue;

// DO NOT IMPORT java.util.Queue; use Queue instead
public class StackClass<T> {

    // Two internal queues
    private Queue<T> q1;
    private Queue<T> q2;

    // To maintain current number of elements
    int size;

    // TODO: complete the methods below using the 2 internal queues
    public StackClass() {
        size = 0;
        q1 = new Queue<>();
        q2 = new Queue<>();
    }

    public void push(T x)
    {
        q1.enqueue(x);
        size++;
    }

    // stacks should be LIFO -- not FIFO, queues are FIFO -- not LIFO
    public T pop()
    {
        if (!q1.isEmpty()) {

            size--;
            while (q1.size() > 1 && q1.peek() != null)
                q2.enqueue(q1.dequeue());

            T ret = q1.dequeue();
            q1 = q2;
            q2 = new Queue<>();

            return ret;
        }
        return null;
    }

    public T peek()
    {
        return q1.peekLast(); // TODO: this needs to be changed
    }

    public int size() {

        return size; // TODO: this needs to be changed
    }

    public boolean isEmpty()
    {
        return size() == 0;
    }

    // END OF TODO

    public void print()
    {
        Queue qp1 = q1;
        Queue qp2 = q2;

        System.out.print("\nQueue 1: ");
        while (!qp1.isEmpty())
            System.out.print(" " + qp1.dequeue());

        System.out.print("\nQueue 2: ");
        while (!qp2.isEmpty())
            System.out.print(" " + qp2.dequeue());
    }

    // a simple driver code
    public static void main(String[] args)
    {
        StackClass s = new StackClass();
        s.push(-1);
        s.push(12);
        s.push(30);

        s.print();

        s.pop();

        System.out.println("\ncurrent size: " + s.size());
        System.out.println(s.peek());
        s.pop();
        System.out.println(s.peek());
        s.pop();
        System.out.println(s.peek());

        System.out.println("current size: " + s.size());

        System.out.print("\n=============================");
        s.print();
    }
}