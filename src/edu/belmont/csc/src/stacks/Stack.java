package edu.belmont.csc.src.stacks;

import java.util.EmptyStackException;
// we are using Java's LinkedList to implement our generic Stack class
import java.util.LinkedList;

public class Stack<T> {

    // TODO: Declare needed class variables
    private LinkedList<T> storage;
    // END OF TODO

    public Stack () {
        this(null);
    }

    // TODO: Add a second constructor that initializes the stack with a given element
    public Stack(T element)
    {
        storage = new LinkedList<>();
        if (element != null)
            storage.add(element);
    }

    // END OF TODO

    // TODO: Complete all the methods below
    public int size()
    {
        return storage.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }


    // implement push below
    public void push(T element)
    {
        storage.addLast(element);
    }

    // implement pop below
    public T pop()
    {
        if (isEmpty()) throw new EmptyStackException();
        return (T) storage.removeLast();
    }

    // implement peek below
    public T peek()
    {
        if (isEmpty())
            return null;

        return (T) storage.peekLast();
    }
    // END OF TODO
}