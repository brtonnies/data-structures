package edu.belmont.csc.src.stacks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Postfix {
    static String[] OPERATORS = {"+", "-", "*", "/", "^"};
    String filepath;
    StackClass<Float> stack;

    public Postfix()
    {
        // if this path is still invalid, that means I forgot to change it back XD
        this(System.getProperty("user.dir") + "/src/blerp/edu.belmont.csc.src.structures/stacks/sample.txt");
    }

    /**
     * Constructor!
     * @param filepath is the path to the file containing the postfix expressions to evaluate.
     *
     * Note: the constructor checks whether there is a file at 'filepath', then tries
     *       to locate a ".txt" file in the same directory as this code in the event
     *       the provided path does not point to an existing file.  if no file is found
     *       then the filepath is set to an empty string, and instead must be provided
     *       directly in any calls to the 'readFile' method.
     */
    public Postfix(String filepath)
    {
        stack = new StackClass<>();

        File sample = new File(filepath);
        if (!sample.exists())
            this.filepath = getTextFilePath();
        else
            this.filepath = filepath;
    }

    public String getTextFilePath()
    {
        File[] files = new File(
                System.getProperty("user.dir") + "/src/"
                        + Postfix.class.getPackageName().replace('.','/')
        ).listFiles((dir, name) -> name.endsWith(".txt"));

        return files != null ? files[0].getPath() : "";
    }

    // method for calculating the postfix expression
    public float evaluate(String expression) throws Exception {
        if (expression.equals(""))
            throw new Exception("==ERR==");

        for(String c : expression.split(" "))
            if (isOperator(c)) {
                if (stack.size() < 2 || stack.size() > 2) {
                    while(!stack.isEmpty())
                        stack.pop();
                    throw new Exception("==ERR==");
                }

                float a = stack.pop();
                float b = stack.pop();
                switch (c) {
                    case "+": {
                        stack.push(a + b);
                        break;
                    }
                    case "-": {
                        stack.push(b - a);
                        break;
                    }
                    case "*": {
                        stack.push(a * b);
                        break;
                    }
                    case "/": {
                        stack.push(b / a);
                        break;
                    }
                    case "^": {
                        stack.push((float) Math.pow(b, a));
                        break;
                    }
                }
            } else
                stack.push(Float.parseFloat(c));

        return stack.pop();
    }

    public boolean isOperator(String c)
    {
        for (String operator : OPERATORS)
            if (operator.equals(c))
                return true;
        return false;
    }

    // method for parsing the expression file
    public void readFile()
    {
        readFile(this.filepath);
    }

    public void readFile(String filepath) {

        Scanner scanner = getScanner(filepath);
        while (scanner != null && scanner.hasNextLine()) {
            String expression = scanner.nextLine();
            try {
                System.out.println(expression + " == " + evaluate(expression));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }

    private Scanner getScanner(String path)
    {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return scanner;
    }

    public static void main(String[] args) {
        Postfix postfix = new Postfix();
        postfix.readFile();             // if the text file to be read is not in the same directory as this code, a file path must be provided here
    }
}
