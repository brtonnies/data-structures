package edu.belmont.csc.src.search;

import edu.belmont.csc.src.queues.Queue;

import java.util.Deque;
import java.util.LinkedList;

public class BinarySearchTree {
    // TODO: Add the internal data according to the driver (main) method so that the compile errors are gone
    Node root;

    // END OF TODO

    // TODO: implement the following traversal methods

    public void preorder(Node root) {
        if (root == null) return;
        System.out.println(root.value);
        preorder(root.left);
        preorder(root.right);
    }

    public void inorder(Node root) {
        if (root == null) return;
        inorder(root.left);
        System.out.println(root.value);
        inorder(root.right);
    }

    public void postorder(Node root) {
        if (root == null) return;
        postorder(root.left);
        postorder(root.right);
        System.out.println(root.value);
    }

    public void levelorder(Node root) {
        Queue<Node> q = new Queue<>();
        q.enqueue(root);

        while (!q.isEmpty()) {
            Node out = q.dequeue();
            System.out.println(out.value);

            if (out.left != null)
                q.enqueue(out.left);

            if (out.right != null)
                q.enqueue(out.right);
        }
    }
    // END OF TODO

    // Driver method
    public static void main(String[] args)
    {
        BinarySearchTree tree = new BinarySearchTree();
        tree.root = new Node(1);
        tree.root.left = new Node(2);
        tree.root.right = new Node(3);
        tree.root.left.left = new Node(4);
        tree.root.left.right = new Node(5);

        System.out.println("Preorder traversal of binary tree is ");
        tree.preorder(tree.root);

        System.out.println("\nInorder traversal of binary tree is ");
        tree.inorder(tree.root);

        System.out.println("\nPostorder traversal of binary tree is ");
        tree.postorder(tree.root);

        System.out.println("\nLevelorder traversal of binary tree is ");
        tree.levelorder(tree.root);
    }
}