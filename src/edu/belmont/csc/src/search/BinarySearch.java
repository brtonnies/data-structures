package edu.belmont.csc.src.search;

import java.util.Random;

public class BinarySearch {
    public static int NUM_GUESSES = 0;
    // after one comparison, ignore half the elements
    // compare x with the middle element
    // if x matches the middle element, return the mid index
    // else if x > midpoint, then x lies in right half of subarray after the middle element
    // els x < midpoint, then x lies in left half of subarray before middle element

    public static int binarySearch(int[] arr, int target, int start, int end) {
        if (arr.length == 0 || start > end)
            return -1;

        boolean ascending = arr[start] < arr[end];

        int mid = start + (end - start)/2; // AVOID Integer.MAX_VALUE

        if (target == arr[mid])
            return mid;

        if (target > arr[mid])
            return ascending ?
                    binarySearch(arr, target, mid+1, end)
                    : binarySearch(arr, target, start,mid-1);

        if (target < arr[mid])
            return ascending ?
                    binarySearch(arr, target, start, mid-1)
                    : binarySearch(arr, target, mid+1, end);

        return -1;
    }

    public static int binarySearch(int[] arr, int target) {
        if (arr.length == 0) return -1;
        int start = 0, end = arr.length - 1;
        int mid = start + (end - start)/2;


        while (start <= end) {
            if (target == arr[mid])
                return mid;
            else if (target > arr[mid])
                start = mid + 1;
            else
                end = mid - 1;

            mid = start + (end - start) / 2;
        }

        return -1;
    }

    /**
     * Given a sorted array in ascending order. It is rotated at some pivot unknown
     * to you. (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]). Find the minimum
     * element (assuming no duplicate exists in the array)
     * @return index of minimum element
     */
    public static int pivotProblem(int[] arr, int start, int end) {
        if (arr.length == 0)
            return -1;

        int mid = start + (end - start)/2;

        if (arr[mid] < arr[start] && arr[mid] < arr[end])
            return arr[start] > arr[end] ? pivotProblem(arr, mid+1, end) : pivotProblem(arr, start, mid-1);;


        if (arr[mid] > arr[start] && arr[mid] > arr[end])
            return arr[start] > arr[end] ? pivotProblem(arr, mid+1, end) : pivotProblem(arr, start, mid-1);


        if (arr[start] <= arr[mid] && arr[start] <= arr[end])
            return start;

        if (arr[end] <= arr[start] && arr[end] <= arr[mid])
            return end;

        if (arr[mid] <= arr[start] && arr[mid] <= arr[end])
            return mid;

        return -1;
    }
    public static int pivotProblem(int[] arr) {
        return pivotProblem(arr, 0, arr.length-1);
    }


    /**
     * Guessing problem!
     */
    public static int numberGuess(int target, int min, int max) {
        NUM_GUESSES++;
        int med = min + (max - min)/2;

        if (med == target)
            return med;

        if (med < target)
            return numberGuess(target, med+1, max);
        else
            return numberGuess(target, min, med-1);
    }



    public static void main(String[] args) {
        int[] arr1 = { 1, 4, 5, 7, 9, 11, 14 };
        int[] arr2 = { 14, 11, 9, 7, 5, 4, 1 };
        int[] pivot1 = { 4, 5, 6, 7, 0, 1, 2 };
        int[] pivot2 = { 2, 1, 0, 7, 6, 5, 4 };
        int t = 4;

        System.out.println("Index of " + t + " (ascending order, recursive): " + binarySearch(arr1, t, 0, 6));
        System.out.println("Index of " + t + " (descending order, recursive): " + binarySearch(arr2, t, 0, 6));
        System.out.println("Index of " + t + " (ascending order, linear): " + binarySearch(arr1, t));

        System.out.println();
        System.out.println("Pivot Problem -- Ascending Order Min Index: " + pivotProblem(pivot1));
        System.out.println("Pivot Problem -- Descending Order Min Index: " + pivotProblem(pivot2));


        Random generator = new Random();
        int max = 1000;
        int cc = generator.nextInt(max) + 1;
        System.out.println("Computer picked: " + cc);
        System.out.println("Number Guessed: " + numberGuess(cc, 1, max) + " in " + NUM_GUESSES + " guesses!");
    }
}
