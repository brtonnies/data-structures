package edu.belmont.csc.src.search;

// TODO: Complete the Node class based on the driver method in BinarySearchTree
public class Node{
    int value;
    Node left = null;
    Node right = null;

    public Node(int value) {
        this.value = value;
    }
}
// END OF TODO
