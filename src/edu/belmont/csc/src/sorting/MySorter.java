package edu.belmont.csc.src.sorting;

public class MySorter {

    public static class BucketSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet)
        {
            int n = arr.length;
            Object[] buckets = new Object[n];
        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class BubbleSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {
            for(int i = 0; i < arr.length - 1; i++) {
                boolean swapped = false;
                for (int j = 1; j < arr.length - i; j++) {
                    swapped = arr[j-1] > arr[j];
                    if (swapped) {
                        int t = arr[j-1];
                        arr[j-1] = arr[j];
                        arr[j] = t;
                    }
                }
                if (!swapped)
                    break;
            }
        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class SelectionSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {
            for(int i = 0; i < arr.length - 1; i++) {
                int idx = i;
                for(int j = i; j < arr.length; j++)
                    if (arr[j] < arr[idx])
                        idx = j;

                int min = arr[idx];
                arr[idx] = arr[i];
                arr[i] = min;
            }
        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class MyInsertionSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {
            for(int i = 1; i < arr.length; i++) {
                int j = i - 1;
                int k = arr[i];

                while (j >= 0 && arr[j] > k)
                {
                    arr[j+1] = arr[j];
                    j--;
                }
                arr[j+1] = k;
            }
        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class MergeSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {

        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class ShellSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {

        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class RadixSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {

        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class QuickSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {

        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    public static class HeapSort extends AbstractSorter {
        public void sort(int[] arr, boolean quiet) {

        }
        public void sort(int[] arr)
        {
            sort(arr, false);
        }
    }

    // Driver code
    public static void main(String[] args)
    {
        int[] arr1 = { 8, 4, 5, 3, 2, 7, 1, 0, 10, 9 };

        AbstractSorter insertionSort = new edu.belmont.csc.src.sorting.Sorter.InsertionSort();
        insertionSort.runSorter(arr1);

        System.out.println();

        int[] arr2 = { 10, 9, 8, 4, 5, 3, 2, 7, 1 };

        AbstractSorter selectionSort = new edu.belmont.csc.src.sorting.Sorter.SelectionSort();
        selectionSort.runSorter(arr2);

        System.out.println();

        int[] arr3 = { 9, 2, 8, 10, 4, 5, 3, 2, 7, 1 };

        AbstractSorter bubbleSort = new edu.belmont.csc.src.sorting.Sorter.BubbleSort();
        bubbleSort.runSorter(arr3);

    }

}
