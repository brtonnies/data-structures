package edu.belmont.csc.src.sorting;

/**
 * Author: Dana Zhang
 * Date: 1/27/2020
 */
abstract class GenericAbstractSorter<T> {
    public abstract void sort(T[] arr);

    public void runSorter(T[] arr, boolean quiet) {
        if (!quiet) {
            System.out.format("Before %s: ", this.getClass().getSimpleName());
            printArray(arr);
            System.out.format("After %s: ", this.getClass().getSimpleName());
        }
        sort(arr);
        if (!quiet)
            printArray(arr);
    }

    public void runSorter(T[] arr)
    {
        runSorter(arr, false);
    }

    private void printArray(T[] arr)
    {
        for (T e: arr) {
            System.out.print(e + " ");
        }
        System.out.println();
    }
}
