package edu.belmont.csc.src.sorting;

/**
 * Author: Dana Zhang
 * Date: 1/27/2020
 * Updated: 1/30/2020 By Brian Tonnies for Quiet edu.belmont.csc.src.sorting
 */
abstract class AbstractSorter {
    public abstract void sort(int[] arr);
    //public abstract void sort(int[] arr, boolean quiet);

    public void runSorter(int[] arr, boolean quiet) {
        if (!quiet) {
            System.out.format("Before %s: ", this.getClass().getSimpleName());
            printArray(arr);
            System.out.format("After %s: ", this.getClass().getSimpleName());
        }
        sort(arr);
        if (!quiet)
            printArray(arr);
    }

    public void runSorter(int[] arr)
    {
        runSorter(arr, false);
    }

    private void printArray(int[] arr)
    {
        for (int e: arr) {
            System.out.print(e + " ");
        }
        System.out.println();
    }
}
