package edu.belmont.csc.src.sorting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class Sorter {

    public static class BubbleSort extends AbstractSorter {
        public void sort(int[] arr) {
            for(int i = 0; i < arr.length - 1; i++) {
                boolean swapped = false;
                for (int j = 1; j < arr.length - i; j++) {
                    swapped = arr[j-1] > arr[j];
                    if (swapped) {
                        int t = arr[j-1];
                        arr[j-1] = arr[j];
                        arr[j] = t;
                    }
                }
                if (!swapped)
                    break;
            }
        }
    }

    public static class SelectionSort extends AbstractSorter {
        public void sort(int[] arr) {
            for(int i = 0; i < arr.length - 1; i++) {
                int idx = i;
                for(int j = i; j < arr.length; j++)
                    if (arr[j] < arr[idx])
                        idx = j;

                int min = arr[idx];
                arr[idx] = arr[i];
                arr[i] = min;
            }
        }
    }

    public static class InsertionSort extends AbstractSorter {
        public void sort(int[] arr) {
            for(int i = 1; i < arr.length; i++) {
                int j = i - 1;
                int key = arr[i];

                while (j >= 0 && arr[j] > key) {
                    arr[j+1] = arr[j];
                    j--;
                }
                arr[j+1] = key;
            }
        }

    }

    public static class ShellSort extends AbstractSorter {
        public void sort(int[] arr) {
for (int gap = arr.length / 2; gap > 0; gap /= 2) {
    for(int i = gap; i < arr.length; i++) {
        int key = arr[i];
        int j = i;

        while (j >= gap && arr[j - gap] > key) {
            arr[j] = arr[j - gap];
            j -= gap;
        }
        arr[j] = key;
    }
}
        }
    }

    public static class RadixSort extends AbstractSorter {
        public void sort(int[] arr) { sort(arr, 10); }

        public void sort(int[] arr, int radix) {
            int max = getMax(arr);

            int exp = 1;
            while(max / exp > 1) {
                // do a bucket-sort like thing...
                int bi;
                int[] buckets = new int[radix];
                int[] output = new int[arr.length];

                for (int i = 0; i < radix; i++)
                    buckets[i] = 0;

                for (int i = 0; i < arr.length; i++) {      // so this is a cool way to not have to use a two dimensional array!
                    bi = (arr[i] / exp) % radix;            // find the value at the current digit
                    buckets[bi]++;                          // increment the count at that position in the buckets
                }

                for (int i = 1; i < radix; i++)             // this is important for maintaining the array values
                    buckets[i] += buckets[i - 1];

                for (int i = arr.length - 1; i >= 0; i--) {
                    bi = (arr[i] / exp) % radix;
                    output[--buckets[bi]] = arr[i];
                }

                for (int i = 0; i < arr.length; i++)
                    arr[i] = output[i];

                exp *= radix;
            }
            char[] digits = new char[5];
            Stack<Character> stack = new Stack<>();

        }

        private int numDigits(int n) {
            // THIS IS SO UGLY BUT SO FAST!
            return n < 100000 ? n < 100 ? n < 10 ? 1 : 2
                    : n < 1000 ? 3 : n < 10000 ? 4 : 5
                        : n < 10000000 ? n < 1000000 ? 6 : 7
                            : n < 100000000 ? 8 : n < 1000000000 ? 9 : 10;
        }
        private int getMax(int[] arr) {
            int max = arr[0];

            for(int i = 1; i < arr.length; i++)
                if (arr[i] > max)
                    max = arr[i];

            return max;
        }
        private int getMin(int[] arr) {
            int min = arr[0];

            for(int i = 1; i < arr.length; i++)
                if (arr[i] < min)
                    min = arr[i];

            return min;
        }
        public void print(String output) {
            System.out.println(output);
        }
        public void print(int[] arr) {
            System.out.print("\n[ " + arr[0]);
            for(int i = 1; i < arr.length; i++)
                System.out.print(", " + arr[i]);
            System.out.print(" ]\n");
        }
    }

    public static class BucketSort {
        public void sort(float[] arr) {
            ArrayList[] buckets = new ArrayList[arr.length];
            for(int i = 0; i < arr.length; i++)
                buckets[i] = new ArrayList<>();

            for (float v : arr)
                buckets[(int) (arr.length * v)].add(v);

            // sort the buckets with a modified insertion sort
            for(ArrayList bucket : buckets) {
                for(int i = 1; i < bucket.size(); i++) {
                    int j = i - 1;
                    float key = (float) bucket.get(i);

                    while (j >= 0 && arr[j] > key) {
                        bucket.set(j+1, bucket.get(j));
                        j--;
                    }
                    bucket.set(j+1, key);
                }
            }

            // merge the buckets back into the original array
            int idx = 0;
            for(int i = 0; i < buckets.length; i++)
                for(Object b : buckets[i])
                    arr[idx++] = (float) b;

        }

        public void runSorter(float[] arr, boolean quiet) {
            if (!quiet) {
                System.out.format("Before %s: ", this.getClass().getSimpleName());
                printArray(arr);
                System.out.format("After %s: ", this.getClass().getSimpleName());
            }
            sort(arr);
            if (!quiet)
                printArray(arr);
        }
        public void runSorter(float[] arr) {
            runSorter(arr, false);
        }
        private void printArray(float[] arr) {
            for (float e: arr) {
                System.out.print(e + " ");
            }
            System.out.println();
        }

    }

    static class MergeSort extends AbstractSorter {
        public void sort(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }

        void merge(int[] arr, int left, int mid, int right)
        {
            int[] tmp = new int[arr.length];
            int leftIdx = left;       // left subarray start
            int rightIdx = mid+1;     // right subarray start
            int idx = leftIdx;        // next index open in temp array

            while (leftIdx <= mid && rightIdx <= right)
                tmp[idx++] = arr[leftIdx] < arr[rightIdx] ? arr[leftIdx++] : arr[rightIdx++];

            //  Copy remaining elements from left subarray, if any
            while (leftIdx <= mid)
                tmp[idx++] = arr[leftIdx++];

            //  Copy remaining elements from second subarray, if any
            while (rightIdx <= right)
                tmp[idx++] = arr[rightIdx++];

            //  Copy merged array into original array
            for (idx = left; idx <= right; idx++)
                arr[idx] = tmp[idx];
        }


        void sort(int[] arr, int left, int right)
        {
            if (left < right) {
                int mid = (left + right) / 2;
                sort(arr, left, mid);
                sort(arr, mid+1, right);
                merge(arr, left, mid, right);
            }
        }

    }

    static class QuickSort extends AbstractSorter {
        public void sort(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }

        int partition(int[] arr, int start, int end)
        {
            int pivot = arr[end];
            int pi = start;
            int i;
            for(i = start; i < end; i++) {
                if (arr[i] < pivot) {
                    int tmp = arr[pi];
                    arr[pi] = arr[i];
                    arr[i] = tmp;
                    pi++;
                }
            }

            int tmp = arr[pi];
            arr[pi] = arr[i];
            arr[i] = tmp;

            return pi; // returns partition index
        }

        void sort(int[] arr, int start, int end)
        {
            if (start < end) {
                int pi = partition(arr, start, end);
                sort(arr, start, pi-1);
                sort(arr, pi+1, end);
            }
        }
    }

    static class HeapSort extends AbstractSorter {
        /******** DO NOT MODIFY THIS METHOD ************/
        public void sort(int[] arr) {
            heapsort(arr);
        }
        /***********************************************/

        void heapsort(int[] arr)
        {
            int n = arr.length;
            for(int i = n/2 - 1; i >= 0; i--)
                heapify(arr, n, i);

            for(int i = n - 1; i >= 0; i--) {
                int tmp = arr[0];
                arr[0] = arr[i];
                arr[i] = tmp;
                heapify(arr, i, 0);
            }
        }

        void heapify(int[] arr, int n, int i)
        {
            int largest = i;
            int l = 2*i + 1;
            int r = 2*i + 2;

            if (l < n && arr[l] > arr[largest])
                largest = l;
            if (r < n && arr[r] > arr[largest])
                largest = r;

            if (largest != i) {
                int tmp = arr[i];
                arr[i] = arr[largest];
                arr[largest] = tmp;
                heapify(arr, n, largest);
            }

        }


        public static void reverseString(char[] s) {
            HashMap<Integer, Integer> map = new HashMap<>();



        }

    }


    // Driver code
    public static void main(String[] args) {
        System.out.println();
        int[] arr1 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};
        AbstractSorter insertionSort = new InsertionSort();
        insertionSort.runSorter(arr1);

        System.out.println();
        int[] arr2 = {10, 9, 8, 4, 5, 3, 2, 7, 1};
        AbstractSorter selectionSort = new SelectionSort();
        selectionSort.runSorter(arr2);

        System.out.println();
        int[] arr3 = {9, 2, 8, 10, 4, 5, 3, 2, 7, 1};
        AbstractSorter bubbleSort = new BubbleSort();
        bubbleSort.runSorter(arr3);

        System.out.println();
        int[] arr4 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};
        ShellSort shellSort = new ShellSort();
        shellSort.runSorter(arr4);

        System.out.println();
        float[] arr5 = { 0.16F, 0.92F, 0.24F, 0.73F, 0.55F, 0.62F, 0.87F, 0.78F, 0.13F };
        BucketSort bucketSort = new BucketSort();
        bucketSort.runSorter(arr5);

        System.out.println();
        int[] arr6 = {4352, 123, 532, 89, 708, 1536, 333, 208, 625, 995, 64};
        RadixSort radixSort = new RadixSort();
        radixSort.runSorter(arr6);

        System.out.println();
        int[] arr7 = {9, 2, 8, 10, 4, 5, 3, 2, 7, 1};
        MergeSort mergeSort = new MergeSort();
        mergeSort.runSorter(arr7);

        System.out.println();
        int[] arr8 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};
        QuickSort quickSort = new QuickSort();
        quickSort.runSorter(arr8);

        System.out.println();
        int[] arr9 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};
        HeapSort heapSort = new HeapSort();
        heapSort.runSorter(arr8);
    }

}