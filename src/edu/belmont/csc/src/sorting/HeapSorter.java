package edu.belmont.csc.src.sorting;


public class HeapSorter {

    static class HeapSort extends AbstractSorter {
        /******** DO NOT MODIFY THIS METHOD ************/
        public void sort(int[] arr) {
            heapsort(arr);
        }
        /***********************************************/

        // TODO: Implement heapsort and heapify
        void heapsort(int[] arr)
        {
            int n = arr.length;
            for(int i = n/2 - 1; i >= 0; i--)
                heapify(arr, n, i);

            for(int i = n - 1; i >= 0; i--) {
                int tmp = arr[0];
                arr[0] = arr[i];
                arr[i] = tmp;
                heapify(arr, i, 0);
            }
        }

        void heapify(int[] arr, int n, int i)
        {
            int largest = i;
            int l = 2*i + 1;
            int r = 2*i + 2;

            if (l < n && arr[l] > arr[largest])
                largest = l;
            if (r < n && arr[r] > arr[largest])
                largest = r;

            if (largest != i) {
                int tmp = arr[i];
                arr[i] = arr[largest];
                arr[largest] = tmp;
                heapify(arr, n, largest);
            }

        }
        // END OF TODO

    }

    // Driver code
    public static void main(String[] args) {
        int[] arr1 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};
        int[] arr2 = {9, 2, 8, 10, 4, 5, 3, 2, 7, 1};

        HeapSort heapSort = new HeapSort();
        heapSort.runSorter(arr1);
        heapSort.runSorter(arr2);

        // TODO: make up some of your own tests

    }

}