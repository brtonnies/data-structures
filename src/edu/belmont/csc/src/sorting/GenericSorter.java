package edu.belmont.csc.src.sorting;

public class GenericSorter {

    public static class BucketSort<T extends Comparable<T>> extends GenericAbstractSorter<T> {
        public void sort(T[] arr)
        {
            int n = arr.length;
            Object[] buckets = new Object[n];
        }
    }

    public static class BubbleSort<T extends Comparable<T>> extends GenericAbstractSorter<T> {
        public void sort(T[] arr) {
            for(int i = 0; i < arr.length - 1; i++) {
                boolean swapped = false;
                for (int j = 1; j < arr.length - i; j++) {
                    //swapped = arr[j-1] > arr[j];
                    swapped = arr[j-1].compareTo(arr[j]) > 0;
                    if (swapped) {
                        T t = arr[j-1];
                        arr[j-1] = arr[j];
                        arr[j] = t;
                    }
                }
                if (!swapped)
                    break;
            }
        }
    }

    public static class SelectionSort<T extends Comparable<T>> extends GenericAbstractSorter<T> {
        public void sort(T[] arr) {
            for(int i = 0; i < arr.length - 1; i++) {
                int idx = i;
                for(int j = i; j < arr.length; j++)
                    //if (arr[j] < arr[idx])
                    if (arr[j].compareTo(arr[idx]) < 0)
                        idx = j;

                T min = arr[idx];
                arr[idx] = arr[i];
                arr[i] = min;
            }
        }
    }

    public static class InsertionSort<T extends Comparable<T>> extends GenericAbstractSorter<T> {
        public void sort(T[] arr) {
            for(int i = 1; i < arr.length; i++) {
                int j = i - 1;
                T k = arr[i];

                //while (j >= 0 && arr[j] > k)
                while (j >= 0 && arr[j].compareTo(k) > 0)
                {
                    arr[j+1] = arr[j];
                    j--;
                }
                arr[j+1] = k;
            }
        }

    }

    // Driver code
    public static void main(String[] args)
    {
        Integer[] arr1 = { 8, 4, 5, 3, 2, 7, 1, 0, 10, 9 };

        GenericAbstractSorter<Integer> insertionSort = new InsertionSort<>();
        insertionSort.runSorter(arr1);

        System.out.println();

        Integer[] arr2 = { 10, 9, 8, 4, 5, 3, 2, 7, 1 };

        GenericAbstractSorter<Integer> selectionSort = new SelectionSort<>();
        selectionSort.runSorter(arr2);

        System.out.println();

        Integer[] arr3 = { 9, 2, 8, 10, 4, 5, 3, 2, 7, 1 };

        GenericAbstractSorter<Integer> bubbleSort = new BubbleSort<>();
        bubbleSort.runSorter(arr3);

    }

}
