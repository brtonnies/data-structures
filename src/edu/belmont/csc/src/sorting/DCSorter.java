package edu.belmont.csc.src.sorting;

public class DCSorter {

    static class MergeSort extends AbstractSorter {
        /******** DO NOT MODIFY THIS METHOD ************/
        public void sort(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }
        /***********************************************/

        // TODO: Implement merge and (recursive) sort
        void merge(int[] arr, int left, int mid, int right)
        {
            int[] tmp = new int[arr.length];
            int leftIdx = left;       // left subarray start
            int rightIdx = mid+1;     // right subarray start
            int idx = leftIdx;        // next index open in temp array

            while (leftIdx <= mid && rightIdx <= right)
                tmp[idx++] = arr[leftIdx] < arr[rightIdx] ? arr[leftIdx++] : arr[rightIdx++];

            //  Copy remaining elements from left subarray, if any
            while (leftIdx <= mid)
                tmp[idx++] = arr[leftIdx++];

            //  Copy remaining elements from second subarray, if any
            while (rightIdx <= right)
                tmp[idx++] = arr[rightIdx++];

            //  Copy merged array into original array
            for (idx = left; idx <= right; idx++)
                arr[idx] = tmp[idx];
        }


        void sort(int[] arr, int left, int right)
        {
            if (left < right) {
                int mid = (left + right) / 2;
                sort(arr, left, mid);
                sort(arr, mid+1, right);
                merge(arr, left, mid, right);
            }
        }
        // END OF TODO

    }

    static class QuickSort extends AbstractSorter {
        /******** DO NOT MODIFY THIS METHOD ************/
        public void sort(int[] arr) {
            sort(arr, 0, arr.length - 1);
        }
        /***********************************************/

        int partition(int[] arr, int start, int end)
        {
            int pivot = arr[end];
            int pi = start;
            int i;
            for(i = start; i < end; i++) {
                if (arr[i] < pivot) {
                    int tmp = arr[pi];
                    arr[pi] = arr[i];
                    arr[i] = tmp;
                    pi++;
                }
            }

            int tmp = arr[pi];
            arr[pi] = arr[i];
            arr[i] = tmp;

            return pi; // returns partition index
        }

        void sort(int[] arr, int start, int end)
        {
            if (start < end) {
                int pi = partition(arr, start, end);
                sort(arr, start, pi-1);
                sort(arr, pi+1, end);
            }
        }
    }

    // Driver code
    public static void main(String[] args) {
        int[] arr1 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};
        int[] arr2 = {8, 4, 5, 3, 2, 7, 1, 0, 10, 9};

        MergeSort mergeSort = new MergeSort();
        mergeSort.runSorter(arr1);

        System.out.println();

        QuickSort quickSort = new QuickSort();
        quickSort.runSorter(arr2);
    }

}