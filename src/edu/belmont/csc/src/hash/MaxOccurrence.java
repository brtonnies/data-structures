package edu.belmont.csc.src.hash;

import java.util.HashMap;

public class MaxOccurrence {
    String input;
    HashMap<Character, Integer> occurrence;

    public MaxOccurrence(String input) {
        this.input = input;
        this.occurrence = new HashMap<>();

        parse();
    }

    public MaxOccurrence() {
        this("wa;ovn;sjkds;aks;djjf;klajsajdf;oiuh34ppq92epfoji;akjfbvhashi;aodifljkwnebpi1p3iphn");
    }

    void parse(String input) {
        for(char c : input.toCharArray()) {
            if (occurrence.containsKey(c))
                occurrence.replace(c,  occurrence.get(c)+1);
            else
                occurrence.put(c, 1);
        }
    }

    void parse() {
        parse(this.input);
    }

    char maxKey() {
        char maxKey = (char) occurrence.keySet().toArray()[0];
        for (char c : occurrence.keySet()) {
            if (occurrence.get(c) > occurrence.get(maxKey))
                maxKey = c;
            else if (occurrence.get(c).equals(occurrence.get(maxKey))) {
                maxKey = (int) c < (int) maxKey ? c : maxKey;
            }

        }

        return maxKey;
    }

    void print() {
        System.out.println(maxKey() + " " + occurrence.get(maxKey()));
    }

    public static void main(String[] args) {
        String input = "wa;ovn;sjkds;aks;djjf;klajsajdf;oiuh34ppq92epfoji;akjfbvhashi;aodifljkwnebpi1p3iphn";

        MaxOccurrence mo = new MaxOccurrence("aaaaAAAA");
        mo.print();

    }
}
