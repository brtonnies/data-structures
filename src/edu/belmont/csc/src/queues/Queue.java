package edu.belmont.csc.src.queues;

import java.util.EmptyStackException;
import java.util.LinkedList;

public class Queue<T> {
    LinkedList<T> list;

    public Queue(){
        this(null);
    }

    public Queue(T firstElement) {
        list = new LinkedList<>();
        if (firstElement != null)
            list.add(firstElement);
    }

    // TODO: implement the following 5 methods. Hint: refer to the Stack class from our last lecture to help you define the method headers

    // enqueue
    public void enqueue(T elem)
    {
        list.addFirst(elem);
    }

    // dequeue
    public T dequeue()
    {
        if (isEmpty()) throw new EmptyStackException();
        return list.removeLast();
    }

    // peek
    public T peek()
    {
        if (isEmpty())
            return null;

        return list.peekLast();
    }

    // peekLast
    public T peekLast()
    {
        if (isEmpty())
            return null;

        return list.peekFirst();
    }

    // size
    public int size()
    {
        return list.size();
    }

    // isEmpty
    public boolean isEmpty()
    {
        return size() == 0;
    }

    // clear the queue
    public void clear() {
        while (!isEmpty())
            dequeue();
    }

}