package edu.belmont.csc.src.queues;

import edu.belmont.csc.src.arrays.DynamicArray;

/**
 * Uses binary heap -- heap => tree-based structure that satisfies the heap invariant:
 *    -- If A is a parent node of B then the priority of B is no higher than the priority of A
 *    -- Must be a complete binary tree -- all nodes except possibly the last -- are completely filled
 *       and as far left as possible
 *    -- Tree:
 *      -- Nodes!
 *          -- left child  => 2i + 1
 *          -- right child => 2i + 2
 *      -- Operations/Methods:
 *          -- Insertion -- add at bottom level (bubble up as necessary)
 *          -- Polling   -- remove root by swapping with last node in the tree, removing, then bubbling down
 *          -- Remove    -- linear scan for node to remove, then basically same operation as polling
 */
public class PriorityQueue<T extends Comparable<T>> {
    DynamicArray<T> heap;

    public PriorityQueue() {
        heap = new DynamicArray<>();
    }

    public void add(T node) {
        heap.add(node);
        bubbleUp(heap.size() - 1);
    }

    public void add(T[] nodes) {
        for (T node : nodes)
            add(node);
    }

    public void bubbleUp(int i) {
        T node = heap.get(i);

        if (i > 0) {
            int parentIdx = i % 2 == 0 ? (i-2)/2 : (i-1)/2;
            // check if it's less than its parent and swap
            T parent = parent(i);
            if (node.compareTo(parent) < 0) {
                heap.set(parentIdx, node);
                heap.set(i, parent);
                bubbleUp(parentIdx);
            }
        }
    }

    public void bubbleDown(int i) {
        T node = heap.get(i);

        if (i < heap.size() && leftChild(i) != null) {
            int leftChildIdx = 2*i + 1;
            T leftChild = heap.get(leftChildIdx);
            if (node.compareTo(leftChild) > 0) {
                heap.set(leftChildIdx, node);
                heap.set(i, leftChild);
                bubbleDown(leftChildIdx);
            }
        }
    }

    public T poll() {
        return removeAt(0);
    }

    public T peek() {
        return heap.get(0);
    }

    public boolean remove(T node) {

        for(int i = 0; i < heap.size(); i++)
            if (node.compareTo(heap.get(i)) == 0) {
                return removeAt(i) == node;
            }

        return false;
    }

    private T removeAt(int i) {
        T node = heap.get(i);
        heap.set(i, heap.get(heap.size() - 1));
        heap.removeAt(heap.size() - 1);
        bubbleDown(i);
        return node;
    }

    public T leftChild(int i) {
        return (heap.size() > 2*i + 1) ? heap.get(2*i + 1) : null;
    }

    public T rightChild(int i) {
        return (heap.size() > 2*i + 2) ? heap.get(2*i + 2) : null;
    }

    public T parent(int i) {
        if (i % 2 == 0)
            return i > 0 ? heap.get((i-2)/2) : null;
        else
            return i > 0 ? heap.get((i-1)/2) : null;
    }

    public int size() {
        return heap.size();
    }

    public DynamicArray<T> ancestors(int i) {
        DynamicArray<T> ancestors = new DynamicArray<>();
        int idx = i;

        while (parent(idx) != null) {
            ancestors.add(parent(idx));
            idx = idx % 2 == 0 ? (idx - 2)/2 : (idx - 1)/2;
        }

        return ancestors;
    }

    public boolean validHeap() {
        int lastIdx = heap.size() - 1;
        T last = heap.get(lastIdx);

        for(int i = heap.size() - 1; i >= 0; i--) {
            last = heap.get(i);
            for (T ancestor : ancestors(i))
                if (last.compareTo(ancestor) > 0)
                    return false;
        }

        return true;
    }

    public boolean isComplete() {
        return isComplete(0);
    }

    private boolean isComplete(int i) {
        boolean rightChildComplete = false;
        boolean leftChildComplete = false;

        if (heap.size() == 1)
            return true;

        if (leftChild(i) != null)
            leftChildComplete = isComplete(2*i + 1);

        if (rightChild(i) != null)
            rightChildComplete = isComplete(2*i + 2);

        // if no children, and i is a left child index, return true
        if (leftChild(i) == null && rightChild(i) == null && i % 2 == 1)
            return true;

        // if no children, and i is a right child index, return true if it has a left sibling
        if (leftChild(i) == null && rightChild(i) == null && i % 2 == 0)
            return heap.get(i-1) != null;

        return (leftChildComplete && rightChildComplete) || (leftChildComplete && rightChild(i) == null);
    }

    public static void main(String[] args) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        //Integer[] arr = { 54, 43, 22, 75, 61, 7, 8, 9, 1 };
        Integer[] arr = {2, 5, 8, -1, 2, 0, 14, 18, -2};
        pq.add(arr);

        System.out.println("  0   1   2  3  4  5   6   7  8");
        System.out.println(pq.heap.toString());
        System.out.println("Complete Tree?: " + pq.isComplete());
        System.out.println("Valid Heap?: " + pq.validHeap());
        System.out.println("Poll: " + pq.poll());
        System.out.println(pq.heap.toString());
        System.out.println("Complete Tree?: " + pq.isComplete());
        System.out.println("Valid Heap?: " + pq.validHeap());
    }
}
