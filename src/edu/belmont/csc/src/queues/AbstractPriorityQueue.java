package edu.belmont.csc.src.queues;


/**
 * Uses binary heap -- heap => tree-based structure that satisfies the heap invariant:
 *    -- If A is a parent node of B then the priority of B is no higher than the priority of A
 *    -- Must be a complete binary tree -- all nodes except possibly the last -- are completely filled
 *       and as far left as possible
 *    -- Tree:
 *      -- Nodes!
 *          -- left child  => 2i + 1
 *          -- right child => 2i + 2
 *      -- Operations/Methods:
 *          -- Insertion -- add at bottom level (bubble up as necessary)
 *          -- Polling -- remove root by swapping with last node in the tree, removing, then bubbling down
 */
abstract class AbstractPriorityQueue {
    public abstract int poll();         // always removes last element (min heap)
    public abstract int peek();             // same deal
    public abstract void add(int node);
    public abstract int remove(int node);   // linear scan to find index of node, then remove like polling operation
}
