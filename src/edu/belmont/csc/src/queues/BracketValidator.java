package edu.belmont.csc.src.queues;

import edu.belmont.csc.src.stacks.Stack;

import java.util.HashMap;

public class BracketValidator {

    // This may help you in the method implementation below
    private HashMap<Character, Character> bracketsMapping;
    private Stack stack;

    public BracketValidator() {
        this.bracketsMapping = new HashMap<>();
        stack = new Stack();

        this.bracketsMapping.put(')', '(');
        this.bracketsMapping.put('}', '{');
        this.bracketsMapping.put(']', '[');
    }

    // TODO: Complete the method below
    /**
     * This method validates whether a sequence of brackets contains all properly matched pairs using the Stack class
     * created in class (**NOT** the Stack class from java.util)
     * @param brackets - a string of a sequence of opening and/or closing brackets (assuming only containing {}[](),
     *                 but if you finish this exercise early, refine your code so that the code works for a string that
     *                 can contain any character(s) in between brackets
     * @return true if all brackets are properly closed or is the string is empty
     */
    public boolean isValid(String brackets) {

        for(char c : brackets.toCharArray()) {

            if (bracketsMapping.values().contains(c)) {
                // if open: push
                stack.push(c);

            } else if (bracketsMapping.keySet().contains(c)) {
                // else closed pop and check if if equals c
                char top = ((char)stack.pop());

                if (top != bracketsMapping.get(c))
                    return false;

            }

            String str = "hi";

        }

        return stack.isEmpty();
    }
    // END OF TODO


    public static void main(String[] args) {
        BracketValidator validator = new BracketValidator();
        System.out.println(validator.isValid("( { hey [ this ] is } valid )"));
    }

}