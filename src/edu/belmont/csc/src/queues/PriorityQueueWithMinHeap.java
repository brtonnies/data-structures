package edu.belmont.csc.src.queues;

import java.util.ArrayList;
import java.util.List;

public class PriorityQueueWithMinHeap {

    private int size;
    private int capacity; // internal capacity of the priority queue
    private List<Integer> heap; // use a dynamic array to represent the internal heap data structure


    /**
     * Default constructor for creating a priority queue
     */
    public PriorityQueueWithMinHeap() {
        heap = new ArrayList<>();
        size = 0;
    }

    /**
     * Constructor of PQ with initial elements
     * @param data - data to initialize the PQ
     */
    public PriorityQueueWithMinHeap(int[] data) {
        this();

        for(int a : data)
            add(a);
    }


    /**
     * Check if the PQ is empty in O(1) time
     * @return true if empty and false otherwise
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Clears everything in the PQ in O(n) time
     */
    public void clear() {
        //heap = new ArrayList<>();
        //heap.clear();
        while(size > 0)
            poll();
    }



    /**
     * Returns the size of the PQ
     * @return size of the PQ
     */
    public int size() {
        return size;
    }


    /**
     * Get element with the highest priority, i.e., the smallest number in the PQ, in O(1) time
     * @return element with the highest priority
     */
    public Integer peek() {
        return size > 0 ? heap.get(0) : null;
    }

    /**
     * Removes element with the highest priority, i.e., the smallest number in the PQ, in O(Log(n)) time
     * Make sure the heap invariant is maintained after this method is called
     * @return element with the highest priority
     */
    public Integer poll() {
        if (heap.size() > 0)
            return removeAt(0);
        return null;
    }



    /**
     * Add a single element to the PQ in O(Log(n)) time
     * Make sure the heap invariant is maintained after this method is called
     * @param element
     */
    public void add(int element) {
        heap.add(element);
        bubbleUp(element);
        size++;
    }



    /**
     * Bubble up the specified element to the proper position in the heap O(Log(n))
     * @param element
     */
    private void bubbleUp(int element) {
        int i = heap.indexOf(element);

        if (i > 0) {
            int parentIdx = i % 2 == 0 ? (i-2)/2 : (i-1)/2;
            int parent = heap.get(parentIdx);

            if (heap.get(parentIdx).compareTo(element) > 0) {
                heap.set(parentIdx, element);
                heap.set(i, parent);
                bubbleUp(element);
            }
        }
    }

    /**
     * Bubble down the specified element to the proper position in the heap O(Log(n))
     * @param element
     */
    private void bubbleDown(int element) {
        int i = heap.indexOf(element);
        Integer leftChild = leftChild(i);
        Integer rightChild = rightChild(i);
        if (leftChild != null && rightChild != null) {
            if (leftChild.compareTo(rightChild) <= 0 && leftChild.compareTo(element) < 0) {
                heap.set(2*i+1, element);
                heap.set(i, leftChild);
                bubbleDown(element);
            } else if (leftChild.compareTo(rightChild) > 0 && rightChild.compareTo(element) < 0) {
                heap.set(2*i+2, element);
                heap.set(i, rightChild);
                bubbleDown(element);
            }
        } else if (leftChild != null && leftChild.compareTo(element) < 0) {
                heap.set(2*i+1, element);
                heap.set(i, leftChild);
                bubbleDown(element);
        }
    }


    // Removes a particular element in the heap, O(n)

    /**
     * Removes the specified element in the PQ in O(n) time
     * @param element
     * @return true if the element was removed successfully and false otherwise
     */
    public boolean remove(int element) {

        boolean removed = false;
        for(int i = 0; i < heap.size(); i++)
            if (heap.get(i).compareTo(element) == 0) {
                Integer r = removeAt(i);
                removed = r != null && r.compareTo(element) == 0;
                break;
            }
        return removed;
    }

    // Removes a node at particular index, O(log(n))

    /**
     * Removes element at the specified index
     * @param index
     * @return value of the element or null if PQ is empty
     */
    private Integer removeAt(int index) {
        if (size == 0 || index >= size || index < 0)
            return null;

        int element = heap.get(index);
        heap.set(index, heap.get(heap.size() - 1));
        heap.set(heap.size() - 1, element);
        heap.remove(heap.size() - 1);
        size--;

        if (size > 0)
            bubbleDown(heap.get(index));

        return element;
    }

    /**
     * MY TESTING METHODS
     */
    private Integer leftChild(int i) {
        return (heap.size() > 2*i + 1) ? heap.get(2*i + 1) : null;
    }

    public Integer rightChild(int i) {
        return (heap.size() > 2*i + 2) ? heap.get(2*i + 2) : null;
    }

    @Override
    public String toString() {
        return heap.toString();
    }

    public static void main(String[] args) {
        int[] arr = {2, 5, 8, -1, 2, 0, 14, 18, -2, 5, 8};
        int[] arr1 = {-2, -1, -2, 0, 2, 5, -4, 2, 8, 14, 18, 5, 5};
        int[] arr2 = {-2, -1, 0, 2, 2, 8, 14, 18, 5};
        PriorityQueueWithMinHeap pq = new PriorityQueueWithMinHeap(arr1);

        System.out.println();

        // TODO: add your own tests for each method
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());


        System.out.println("\nPolling All Elements:");
        while(pq.size() > 0) {
            System.out.println("\nPoll: " + pq.poll());
            System.out.println(pq);
            System.out.println("PQ Size: " + pq.size());
            System.out.println("PQ Empty: " + pq.isEmpty());
        }

        System.out.println("Adding elements back to PQ...");
        for(int i : arr1)
            pq.add(i);
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());

        System.out.println("\nRemove at i=3: " + pq.removeAt(3));
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());


        System.out.println("\nRemove at Invalid i=12: " + pq.removeAt(12));
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());


        System.out.println("\nRemove Element 5: " + pq.remove(5));
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());


        System.out.println("\nRemove Non-existent Element 37: " + pq.remove(0));
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());


        System.out.println("\nEmpty the PQ" );
        pq.clear();
        System.out.println(pq);
        System.out.println("PQ Size: " + pq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());


        PriorityQueueWithMinHeap epq = new PriorityQueueWithMinHeap();
        System.out.println("\nRemove from empty PQ: " + epq.remove(0));
        System.out.println("Poll empty PQ: " + epq.poll());
        System.out.println("PQ Size: " + epq.size());
        System.out.println("PQ Empty: " + pq.isEmpty());

        // END OF TODO
    }
}