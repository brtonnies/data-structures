package edu.belmont.csc.src.strings;

import java.util.HashMap;
import java.util.Map;

public class CommonProblems {

    public static void reverseString(char[] s) {
        int left = 0;
        int right = s.length - 1;

        while (left < right) {
            char tmp = s[left];
            s[left] = s[right];
            s[right] = tmp;

            left++;
            right--;
        }
    }

    public static boolean isPalindrome(String s) {
        int left = 0;
        int right = s.length() - 1;
        s = s.toLowerCase();

        while (left < right) {
            if (s.charAt(left) != s.charAt(right))
                return false;

            left++;
            while (left < right && !Character.isLetterOrDigit(s.charAt(left)))
                left++;

            right--;
            while (left < right && !Character.isLetterOrDigit(s.charAt(right)))
                right--;
        }

        return true;
    }

    public static int longestSubstring(String s) {
        int[] visited = new int[256];
        int longest = 0;
        int curr = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (visited[c] == 0) {
                curr++;
                visited[c] = 1;
            } else {
                if (curr > longest)
                    longest = curr;

                curr = 0;
                visited = new int[256];
                curr++;
                visited[c] = 1;
            }
        }
        return longest;
    }

    public static String reverseWords(String s) {
        char[] sc = s.toCharArray();
        for(String w : s.split("\\s")) {
            int start = s.indexOf(w);
            char[] wc = w.toCharArray();

            int left = 0;
            int right = w.length() - 1;

            while (left < right) {
                char tmp = wc[left];
                wc[left] = wc[right];
                wc[right] = tmp;

                left++;
                right--;
            }

            for(int i = 0; i < wc.length; i++)
                sc[start+i] = wc[i];
        }

        return String.valueOf(sc);
    }

    public static int myAtoi(String str) {
        int index = 0, sign = 1, total = 0;
        if (str.length() == 0)
            return 0;

        while (index < str.length() && str.charAt(index) == ' ')
            index++;

        if (index < str.length() && (str.charAt(index) == '+' || str.charAt(index) == '-')) {
            sign = str.charAt(index) == '+' ? 1 : -1;
            index++;
        }

        if (index < str.length() && !Character.isDigit(str.charAt(index))) return 0;

        int result = 0;
        while (index < str.length()) {
            if (!Character.isDigit(str.charAt(index)))
                break;

            char current = str.charAt(index++);
            int previous = result;
            result *= 10;

            if (previous != result/10)
                return sign == -1 ? Integer.MIN_VALUE : Integer.MAX_VALUE;

            result += (current - '0');

            if (result < 0)
                return sign == -1 ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        }
        return result * sign;
    }

    public static void main(String[] args) {
        String example = "This is a string!";
        char[] ex = example.toCharArray();

        System.out.print("Reversing '" + String.valueOf(ex) + "': ");
        reverseString(ex);
        System.out.println(ex);

        System.out.println("Is 'A man, a plan, a canal: Panama' a palindrome: " + isPalindrome("A man, a plan, a canal: Panama"));

        System.out.print("Longest substring of 'elizabeth': ");
        System.out.println(longestSubstring("elizabeth"));

        System.out.println("Reversing words in 'Let's take LeetCode contest': " + reverseWords("Let's take LeetCode contest"));

    }
}
