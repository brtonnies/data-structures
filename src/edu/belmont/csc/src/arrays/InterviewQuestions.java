package edu.belmont.csc.src.arrays;

import edu.belmont.csc.src.queues.Queue;

public class InterviewQuestions {

    public static void duplicateZeros(int[] arr) {
        Queue<Integer> q = new Queue<>();

        for (int i = 0; i < arr.length; i++)
            q.enqueue(arr[i]);

        int temp;
        for (int i = 0; i < arr.length; i++) {
            temp = q.dequeue();
            if (temp == 0) {
                arr[i] = temp;
                if (i + 1 < arr.length)
                    arr[++i] = temp;

            } else
                arr[i] = temp;

        }
    }

    public static void printArray(int[] arr) {
        System.out.print("[ ");
        for(int i =0; i < arr.length; i++)
            System.out.print(i < arr.length - 1 ? arr[i] + ", " : arr[i]);

        System.out.println(" ]");
    }

    public static void main(String[] args) {
        int[] arr1 = new int[] {1,0,2,3,0,4,5,0};

        printArray(arr1);
        duplicateZeros((arr1));
        printArray(arr1);
    }
}
