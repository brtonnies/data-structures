package edu.belmont.csc.src.arrays;

import java.util.Iterator;

public class DynamicArray<T> implements Iterable<T> {
    private int size;
    private int capacity;
    private T[] arr;

    public DynamicArray(int capacity) {
        if (capacity < 0)
            throw new IllegalArgumentException("Illegal capacity value: " + capacity);
        this.capacity = capacity;
        this.arr = (T[]) new Object[capacity];
        this.size = 0;
    }

    public DynamicArray() {
        this(20);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public T get(int index) {
        checkBounds(index);
        return arr[index];
    }

    public void set(int index, T element) {
        checkBounds(index);
        arr[index] = element;
    }

    public void add(T element) {
        if (size == capacity) {
            capacity = capacity*2;
            T[] newArr = (T[]) new Object[capacity];
            for(int i=0; i < size; i++)
                newArr[i] = arr[i];

            arr = newArr;
        }
        arr[size++] = element;
    }

    public void add(T[] elements)
    {
        for(int i = 0; i < elements.length; i++)
            add(elements[i]);
    }

    public int indexOf(T element) {
        int index = -1;

        for(int i = 0; i < size; i++)
            if (arr[i].equals(element))
                return i;

        return index;
    }

    public T removeAt(int index)
    {
        checkBounds(index);
        T element = arr[index];

        if (contains(element)) {
            size -= 1;
            T[] newArr = (T[]) new Object[capacity];
            for (int i = 0; i < size; i++)
                newArr[i] = i >= index && i + 1 <= size ?
                        arr[i + 1] : arr[i];

            arr = newArr;
        }
        return element;
    }

    public boolean remove(T element)
    {
        removeAt(indexOf(element));

        return !contains(element);
    }

    public boolean contains(T element)
    {
        return indexOf(element) != -1;
    }

    public void clear()
    {
        arr = (T[]) new Object[capacity];
        size = 0;
    }

    public void checkBounds(int index)
    {
        if (index < 0 || index >= size)
            throw new ArrayIndexOutOfBoundsException("Index Out of Bounds: " + index + " not in 0-" + (size-1));
    }

    public Iterator<T> iterator() {
        return new DynamicArrayIterator<T>(this);
    }

    public String toString()
    {
        if (isEmpty()) {
            return "[]";
        } else {
            String output = "[ ";
            for (int i = 0; i < size; i++)
                output += i == size - 1 ? arr[i].toString() : arr[i].toString() + ", ";

            output += " ]";
            return output;
        }
    }

    public String fullOutputStr()
    {
        if (isEmpty()) {
            return "[]";
        } else {
            String output = "[ ";
            for (int i = 0; i < capacity; i++)
                output += i == capacity - 1 ? arr[i].toString() : arr[i].toString() + ", ";

            output += " ]";
            return output;
        }
    }
}
