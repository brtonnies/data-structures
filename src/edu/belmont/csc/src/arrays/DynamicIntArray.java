package edu.belmont.csc.src.arrays;

public class DynamicIntArray {
    private int size;
    private int capacity;
    private int[] arr;

    public DynamicIntArray(int capacity) {
        this.capacity = capacity;
        this.arr = new int[capacity];
        this.size = 0;
    }

    public DynamicIntArray() {
        this(20);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        for(int i = 0; i < size; i++)
            if (arr[i] != 0)
                return false;

        return true;
    }

    public int get(int index) {
        checkBounds(index);
        return arr[index];
    }

    public void set(int index, int element) {
        checkBounds(index);
        arr[index] = element;
    }

    public void add(int element) {
        if (size == capacity) {
            capacity = capacity*2;
            int[] newArr = new int[capacity];
            for(int i=0; i < size; i++)
                newArr[i] = arr[i];

            arr = newArr;
        }
        size += 1;
        arr[size-1] = element;
    }

    public void add(int[] elements)
    {
        for(int i = 0; i < elements.length; i++)
            add(elements[i]);
    }

    public int indexOf(int element) {
        int index = -1;

        for(int i = 0; i < size; i++)
            if (arr[i] == element)
                return i;

        return index;
    }

    public int removeAt(int index)
    {
        checkBounds(index);
        int element = arr[index];

        if (contains(element)) {
            size -= 1;
            int[] newArr = new int[capacity];
            for (int i = 0; i < size; i++)
                newArr[i] = i >= index && i + 1 <= size ?
                        arr[i + 1] : arr[i];

            arr = newArr;
        }
        return element;
    }

    public boolean remove(int element)
    {
        removeAt(indexOf(element));

        return !contains(element);
    }

    public boolean contains(int element)
    {
        for(int i = 0; i < size; i++)
            if (arr[i] == element)
                return true;

        return false;
    }

    public void clear()
    {
        this.arr = new int[capacity];
    }

    public void checkBounds(int index)
    {
        if (index < 0 || index >= size)
            throw new ArrayIndexOutOfBoundsException("Index Out of Bounds: " + index + " not in 0-" + (size-1));
    }

    public String toStr()
    {
        String output = "[ ";
        for(int i = 0; i < size; i++)
            output += i == size - 1 ? arr[i] : arr[i] + ", ";

        output += " ]";
        return output;
    }

    public String fullOutputStr()
    {
        String output = "[ ";
        for(int i = 0; i < capacity; i++)
            output += i == capacity - 1 ? arr[i] : arr[i] + ", ";

        output += " ]";
        return output;
    }
}
