package edu.belmont.csc.src.arrays;

import java.util.Iterator;

public class DynamicArrayIterator<T> implements Iterator<T> {
    DynamicArray<T> arr;
    int currentIdx;

    public DynamicArrayIterator(DynamicArray<T> arr) {
        this.arr = arr;
        this.currentIdx = 0;
    }

    @Override
    public boolean hasNext() {
        return currentIdx < arr.size() - 1;
    }

    @Override
    public T next() {
        return arr.get(++currentIdx);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
