package test.sorting;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.belmont.csc.src.sorting.Sorter.*;

class SorterTest {

    BubbleSort bubbleSort;
    InsertionSort insertionSort;
    SelectionSort selectionSort;
    int[] arr1;
    int[] arr2;
    int[] arr3;

    @BeforeEach
    void setUp() {
        int[] arr1 = { 8, 4, 5, 3, 2, 7, 1, 0, 10, 9 };
        int[] arr2 = { 10, 9, 8, 4, 5, 3, 2, 7, 1 };
        int[] arr3 = { 9, 2, 8, 10, 4, 5, 3, 2, 7, 1 };

        this.arr1 = arr1;
        this.arr2 = arr2;
        this.arr3 = arr3;

        bubbleSort = new BubbleSort();
        insertionSort = new InsertionSort();
        selectionSort = new SelectionSort();
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void bubbleSort()
    {

    }

    @Test
    void insertionSort()
    {

    }

    @Test
    void selectionSort()
    {

    }
}