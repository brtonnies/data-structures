package test.structures.trees;


import edu.belmont.csc.src.trees.BinarySearchTree;
import edu.belmont.csc.src.trees.BinarySearchTree.Node;
import edu.belmont.csc.src.trees.BinaryTreePrinter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTreeTest {
    BinarySearchTree tree;
    int[] values;
    Random generator;

    public static void printTree(BinarySearchTree tree) {
        BinaryTreePrinter printer = new BinaryTreePrinter(tree);
        printer.print(System.out);
    }

    @BeforeEach
    void setUp() {
        tree = new BinarySearchTree();
        int[] arr = { 1, 5, 5, 4, 4, 3, 3, 2, 1, 7, 11, 13, 12, 17, 14, 13 };
        int[] arr1 = {1, 2, 2, 2, 2, 2, 2, 2};
        for(int i : arr)
            tree.insert(i);
    }

    @AfterEach
    void tearDown() {
        //printTree(tree);
    }

    @Test
    void size() {
        assertEquals(11, tree.size());
    }

    @Test
    void isEmpty() {
        assertFalse(tree.isEmpty());

        while (!tree.isEmpty())
            tree.remove(tree.root.value);

        assertTrue(tree.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(tree.contains(1));
        assertTrue(tree.contains(2));
        assertTrue(tree.contains(3));
        assertTrue(tree.contains(4));
        assertTrue(tree.contains(5));
        assertTrue(tree.contains(7));
        assertTrue(tree.contains(11));
        assertTrue(tree.contains(12));
        assertTrue(tree.contains(13));
        assertTrue(tree.contains(14));
        assertTrue(tree.contains(17));
    }

    @Test
    void notContains() {
        assertFalse(tree.contains(100));
        assertFalse(tree.contains(16));
        assertFalse(tree.contains(25));
        assertFalse(tree.contains(6));
        assertFalse(tree.contains(8));
        assertFalse(tree.contains(19));
    }

    @Test
    void insert() {
        tree = new BinarySearchTree();
        int[] arr = { 1, 5, 4, 3, 2, 1, 7, 11, 13, 12, 17, 14, 13 };
        int[] arr1 = {1, 2, 2, 2, 2, 2, 2, 2};

        for (int i : arr) {
            tree.insert(i);
            printTree(tree);
            System.out.println();
            System.out.println("Inserted " + i + " into tree.");
            System.out.println("Success: " + tree.contains(i));
            //System.out.println("Valid tree: " + tree.isValid(tree.root));
            System.out.println("==========================================");
        }
    }

    @Test
    void remove() {
        printTree(tree);
        while (tree.size() > 0) {
            System.out.println("\nRemoving: " + tree.root.value + "...");
            System.out.println("Success: " + tree.remove(tree.root.value));
            printTree(tree);
            System.out.println("\n==========================================\n");
        }

        printTree(tree);
        assertTrue(tree.isEmpty());
        assertEquals(0, tree.size());
    }

    @Test
    void isValid() {
        assertTrue(tree.isValid(tree.root));
    }

    @Test
    void isInvalid() {
//        [2,1,3]
//        [10,5,15,null,null,6,20]
//        [2147483647,null,-2147483648]
//        [-2147483648,-2147483648]

        BinarySearchTree invalid = new BinarySearchTree();
        invalid.root = new BinarySearchTree.Node(10);
        invalid.root.left = new BinarySearchTree.Node(5);
        invalid.root.right = new BinarySearchTree.Node(15);

        BinarySearchTree invalid1 = new BinarySearchTree();
        invalid1.root = new BinarySearchTree.Node(2147483647);
        invalid1.root.right = new BinarySearchTree.Node(-2147483647);

        BinarySearchTree invalid2 = new BinarySearchTree();
        invalid1.root = new BinarySearchTree.Node(-2147483647);
        invalid1.root.left = new BinarySearchTree.Node(-2147483647);

        printTree(invalid1);
        assertFalse(invalid1.isValid(invalid.root));

        printTree(invalid2);
        assertFalse(invalid2.isValid(invalid.root));
    }

}