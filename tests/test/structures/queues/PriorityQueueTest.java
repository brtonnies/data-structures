package test.structures.queues;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.belmont.csc.src.queues.PriorityQueue;

class PriorityQueueTest {
    PriorityQueue<Integer> pq;
    @BeforeEach
    void setUp() {
        pq = new PriorityQueue<>();
        Integer[] arr = {1,2,3,4,5,6,7,8,9};
        pq.add(arr);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void add() {
    }

    @Test
    void bubbleUp() {
    }

    @Test
    void bubbleDown() {
    }

    @Test
    void testAdd() {
    }

    @Test
    void isComplete() {
    }

    @Test
    void testIsComplete() {
    }

    @Test
    void leftChild() {
    }

    @Test
    void rightChild() {
    }

    @Test
    void parent() {
    }

    @Test
    void poll() {
    }

    @Test
    void peek() {
    }

    @Test
    void remove() {
    }
}