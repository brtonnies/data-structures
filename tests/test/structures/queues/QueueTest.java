package test.structures.queues;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import edu.belmont.csc.src.queues.Queue;

class QueueTest {
    Queue queue;

    @BeforeEach
    void setUp()
    {
         queue = new Queue(1);
         queue.enqueue(2);
         queue.enqueue(3);
         queue.enqueue(4);
         queue.enqueue(5);

    }

    @AfterEach
    void tearDown()
    {
        while(!queue.isEmpty())
            System.out.println(queue.dequeue());
    }

    @Test
    void enqueue()
    {
        queue.enqueue(6);
        assertEquals(6, queue.size());
        assertEquals(1, queue.peek());
        queue.enqueue(queue.dequeue());
        assertEquals(2, queue.peek());
    }

    @Test
    void dequeue()
    {
        assertEquals(1, queue.dequeue());
        assertEquals(4, queue.size());
    }

    @Test
    void peek()
    {

    }

    @Test
    void size()
    {
        assertEquals(5, queue.size());
    }

    @Test
    void isEmpty()
    {
        assertFalse(queue.isEmpty());

        while(!queue.isEmpty())
            System.out.println(queue.dequeue());

        assertTrue(queue.isEmpty());
    }
}