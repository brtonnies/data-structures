package test.structures.lists;

import edu.belmont.csc.src.lists.DoublyLinkedList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoublyLinkedListTest {

    DoublyLinkedList list;

    @BeforeEach
    void setUp() {
        list = new DoublyLinkedList<>();
        list.append(1);
        list.append('2');
        list.append(3);
        list.append(4);
        list.append(5);
        list.append(3);
    }

    @AfterEach
    void tearDown() {
        printList();
    }

    @Test
    void append() {
        list.append(1);
        assertEquals(1, list.tail.data);
    }

    @Test
    void insertAtHead() {
        list.insertAtHead(0);
        assertEquals(0,list.head.data);
    }

    @Test
    void clear() {
        list = new DoublyLinkedList<Integer>();
        list.clear();
        assertTrue(list.isEmpty());
    }

    @Test
    void isEmpty() {
        assertFalse(list.isEmpty());
        assertTrue((new DoublyLinkedList<Integer>()).isEmpty());
    }

    @Test
    void removeFromHead() {
        assertEquals(1,list.removeFromHead());
        assertEquals('2', list.head.data);

        DoublyLinkedList<Integer> empty = new DoublyLinkedList<>();
        assertNull(empty.removeFromHead());
    }

    @Test
    void removeFromTail() {
        DoublyLinkedList.Node tail = list.tail;
        assertEquals(tail, list.removeFromTail());

        tail = list.tail;
        assertEquals(tail, list.removeFromTail());

        tail = list.tail;
        assertEquals(tail, list.removeFromTail());

        tail = list.tail;
        assertEquals(tail, list.removeFromTail());

        tail = list.tail;
        assertEquals(tail, list.removeFromTail());

        tail = list.tail;
        assertEquals(tail, list.removeFromTail());

    }

    @Test
    void removeAt() {
        assertEquals(4, list.removeAt(3).data);
    }

    @Test
    void remove() {
        assertTrue(list.remove(5));
        assertEquals(1, list.remove(list.head));
    }

    @Test
    void indexOf() {
        assertEquals(0, list.indexOf(1));
        assertEquals(1, list.indexOf('2'));
        assertEquals(2, list.indexOf(3));
        assertEquals(3, list.indexOf(4));
        assertEquals(4, list.indexOf(5));

        assertEquals(-1, list.indexOf(10));
    }


    public void printList() {
        DoublyLinkedList.Node node = list.head;

        if (node == null && list.tail == null) {
            System.out.println("Empty list.");
        } else {
            while (node.next != null) {
                System.out.println(node.data);
                node = node.next;
            }
            System.out.println(node.data);
        }
    }
}