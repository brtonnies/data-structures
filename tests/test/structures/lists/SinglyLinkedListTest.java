package test.structures.lists;
import edu.belmont.csc.src.lists.SinglyLinkedList;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class SinglyLinkedListTest {

    SinglyLinkedList list;

    @BeforeEach
    void before()
    {
        list = new SinglyLinkedList();
        list.insertAtHead(0);
        list.insertAtHead(1);
        list.insertAtHead(2);
        list.insertAtHead(3);
        list.insertAtHead(4);
        list.insertAtHead(5);

        list.printList();
    }

    @AfterEach
    void after()
    {
        System.out.println("===============================");
        list.printList();
    }


    @Test
    void insertAtHead() {

        list.insertAtHead(6);
        assertEquals(0, list.head.next.next.next.next.next.next.data);
    }

    @Test
    void insertAfter() {
        list.insertAfter(list.head, 4);
        assertEquals(4, list.head.next.data);
    }

    @Test
    void insertAt() {
        list.insertAt(5,2);
        assertEquals(5, list.head.next.next.data);
    }

    @Test
    void append() {
        list.append(2);
        assertEquals(2, list.head.next.next.next.data);
    }

    @Test
    void printList() {
        //list.printList();
    }

    @Test
    void removeFromHead() {
        list.removeFromHead();
        assertEquals(4, list.head.data);
    }

    @Test
    void removeFromTail() {
        list.removeFromTail();
        assertEquals(1, list.head.next.next.next.next.data);
    }

    @Test
    void removeAt() {

        SinglyLinkedList.Node removed = list.removeAt(2);
        assertEquals(3, removed.data);
    }
}