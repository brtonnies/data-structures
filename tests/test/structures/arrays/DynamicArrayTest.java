package test.structures.arrays;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.belmont.csc.src.arrays.DynamicArray;

class DynamicArrayTest {

    DynamicArray dynamicArray;

    @BeforeEach
    void setUp() {
        dynamicArray = new DynamicArray();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void size() {
    }

    @Test
    void isEmpty() {
    }

    @Test
    void get() {
    }

    @Test
    void set() {
    }

    @Test
    void add() {
    }

    @Test
    void testAdd() {
    }

    @Test
    void indexOf() {
    }

    @Test
    void removeAt() {
    }
}