package test.structures.postfix;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.belmont.csc.src.stacks.Postfix;

import static org.junit.jupiter.api.Assertions.*;

class PostfixTest {
    Postfix postfix;

    @BeforeEach
    void before()
    {
        postfix = new Postfix();
    }

    @AfterEach
    void after()
    {

    }

    @Test
    void evaluate()
    {
        try {
            assertEquals(5, postfix.evaluate("3 2 +"));
            assertEquals(-1, postfix.evaluate("2 3 -"));
            assertEquals(6, postfix.evaluate("3 2 *"));
            assertEquals(8, postfix.evaluate("2 3 ^"));
            assertEquals(5, postfix.evaluate("3 4 * 2 + 9 -"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void isOperator()
    {

    }

    @Test
    void readFile()
    {
        String filepath = postfix.getTextFilePath();

        postfix.readFile(System.getProperty("user.dir") + "/src/blerp/edu.belmont.csc.src.structures/stacks/sample.txt");
    }
}