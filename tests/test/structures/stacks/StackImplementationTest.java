package test.structures.stacks;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import edu.belmont.csc.src.stacks.Stack;
import edu.belmont.csc.src.stacks.StackClass;

import static org.junit.jupiter.api.Assertions.*;

class StackImplementationTest {
    StackClass stack1;
    Stack stack2;

    @BeforeEach
    void setUp() {
        stack1 = new StackClass();
        stack2 = new Stack();

        int[] contents = { 1, 2, 3, 4, 5 };

        for(int i = 0; i < contents.length; i++) {
            stack1.push(contents[i]);
            stack2.push(contents[i]);
        }
    }

    void printStacks()
    {
        System.out.println("Stack Implementation With Queues:");
        while (stack1.size() > 0)
            System.out.println(stack1.pop());

        System.out.println("\nStack Implementation With LinkedList:");
        while (stack2.size() > 0)
            System.out.println(stack2.pop());
    }

    @AfterEach
    void tearDown() {
        printStacks();
    }

    @Test
    void size() {
        assertEquals(stack1.size(), stack2.size());
    }

    @Test
    void isEmpty() {
        assertEquals(stack1.isEmpty(), stack2.isEmpty());
    }

    @Test
    void push() {
        stack1.push(6);
        stack2.push(6);

    }

    @Test
    void pop() {
        assertEquals(stack1.pop(), stack2.pop());
    }

    @Test
    void peek() {
        assertEquals(stack1.peek(), stack2.peek());
    }
}