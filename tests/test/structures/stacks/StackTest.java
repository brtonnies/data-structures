package test.structures.stacks;

import edu.belmont.csc.src.queues.BracketValidator;
import edu.belmont.csc.src.stacks.Stack;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {
    Stack stack;
    BracketValidator validator;

    @BeforeEach
    void before()
    {
        stack = new Stack();
        validator = new BracketValidator();

        stack.push(1);
        stack.push(2);
        stack.push(3);

    }

    @AfterEach
    void after()
    {
        while (stack.peek() != null && !stack.isEmpty())
            System.out.println(stack.pop());
    }

    @Test
    void size() {
        assertEquals(3, stack.size());
    }

    @Test
    void isEmpty() {
        assertFalse(stack.isEmpty());
        assertTrue((new Stack()).isEmpty());
    }

    @Test
    void push() {
        stack.push(4);
        assertEquals(4, stack.peek());
    }

    @Test
    void pop() {
        assertEquals(3, stack.pop());
        assertEquals(2, stack.pop());
    }

    @Test
    void peek() {
        assertEquals(3, stack.peek());
        stack.push(4);
        assertEquals(4, stack.peek());
    }

    @Test
    void validatorTest()
    {
        assertTrue(validator.isValid("( hey { this [ is ] so } valid )"));
        assertFalse(validator.isValid("( { [ ) } ] }"));
    }
}