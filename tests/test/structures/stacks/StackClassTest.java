package test.structures.stacks;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import edu.belmont.csc.src.stacks.StackClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackClassTest {

    StackClass stack;

    @BeforeEach
    void before()
    {
        stack = new StackClass();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
    }

    @AfterEach
    void after()
    {
        stack.print();
    }

    @Test
    void push() {
        stack.push(6);
        assertEquals(6, stack.size());
    }

    @Test
    void pop() {

        assertEquals(5, stack.pop());
        assertEquals(4, stack.pop());
    }

    @Test
    void top() {
        assertEquals(5, stack.peek());
        stack.pop();
        assertEquals(4, stack.peek());
    }

    @Test
    void size() {
    }

    @Test
    void main() {
    }
}